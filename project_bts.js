const myqsl = require('mysql');
const express = require('express');
var cors = require('cors');
var app = express();
const jwt = require('jsonwebtoken');
const bodyparser = require('body-parser');

app.use(bodyparser.json());

app.use(cors());
app.options('*', cors());  // enable pre-flight

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, contentType,Content-Type, Accept, Authorization");
    next();
});

var dotenv = require('dotenv');
dotenv.config({ path: ".env" })

var mysqlConnection = myqsl.createConnection({
    host: 'localhost',
    user: 'root',
    password: '12345678',
    //password: 'root',
    database: 'bts',
    // host: process.env.DB_HOST,
    // port: process.env.DB_PORT,
    // user: process.env.DB_USER,
    // password: process.env.DB_PASSWORD,
    // database: process.env.DB_DATABASE,
    multipleStatements: true
});

mysqlConnection.connect((err) => {
    if (!err) console.log('DB connection succeded');
    else console.log('DB connection failed \n Error : ' + JSON.stringify(err, undefined, 2));
});

app.listen(4000, function () {
    console.log('Express server is running at port no : 4000');
    console.log('CORS-enabled web server listening on port 4000');
});
//app.listen(process.env.PORT,()=>console.log('Express server is running at port no : '+process.env.PORT));

//login
app.post('/login', (req, res) => {
    let emp = req.body;
    // Mock User
    const user = {
        email: emp.email,
        password: emp.password
    }

    var sql = 'select count(Id_User) as Id from user where email = ? and password = ?;';

    mysqlConnection.query(sql, [emp.email, emp.password], (err, rows, fields) => {
        if (!err) {
            // res.send(rows);
            console.log(rows[0].Id);
            if (rows[0].Id == 1) {
                jwt.sign({ user }, 'secretkey', /*{expiresIn: '30s'},*/(err, token) => {
                    res.json({
                        token
                    });
                });
            } else {
                res.json(rows);
            }
        }
        else console.log(err);
    })
});

//fungsi untuk verifikasi
function verifyToken(req, res, next) {
    const bearerHeader = req.headers['authorization'];
    if (typeof bearerHeader !== 'undefined') {
        //get token from req Header authorization 
        const bearer = bearerHeader;
        //set the token
        req.token = bearer;
        //middleware
        next();
    }
    else {
        //forbidden
        res.sendStatus(403);
    }
}

//KARYAWAN
// menambahkan karyawan
app.post('/Insert_Karyawan', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err) => {
        if (err) {
            res.sendStatus(403);
        }
        else {
            let emp = req.body;

            var sql = 'SET @NIP = ?; SET @nama = ?;SET @NIK = ?; SET @email = ?; SET @Tempat_Lahir = ?; SET @Tanggal_Lahir = ?; SET @Alamat = ?;\
            SET @NPWP = ?; SET @BPJS_TenagaKerja = ?; SET @BPJS_Kesehatan = ?; SET @Create_at = ?; SET @foto = ?;\
            SET @Id_Jabatan = ?; SET @Alamat_Kerja = ?; SET @Telp_Kerja = ?; SET @Create_by = ?;\
            CALL insertKaryawan(@NIP,@nama,@NIK,@email,@Tempat_Lahir,@Tanggal_Lahir,@alamat,@NPWP,@BPJS_TenagaKerja,@BPJS_Kesehatan,@Create_at,@foto,@Id_Jabatan,@Alamat_Kerja,@Telp_Kerja,@Create_by);';

            mysqlConnection.query(sql, [emp.NIP, emp.Nama, emp.NIK, emp.Email, emp.Tempat_Lahir, emp.Tanggal_Lahir, emp.Alamat, emp.NPWP, emp.BPJS_TenagaKerja, emp.BPJS_Kesehatan, emp.Create_at, emp.Foto, emp.Id_Jabatan, emp.Alamat_Kerja, emp.Telp_Kerja, emp.Create_by], (err, rows, fields) => {
                if (!err)
                    res.send(rows);
                else console.log(err);
            })
        }
    });
});

//get All Karyawan
app.get('/Get_All_Karyawan', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err) => {
        if (err) {
            res.sendStatus(403);
        }
        else {
            var sql = 'select * from karyawan;'

            mysqlConnection.query(sql, (err, rows, fields) => {
                if (!err) {
                    res.send(rows);
                }
                else console.log(err);
            });
        }
    });
});

//update Karyawan (in_progress)
app.put('/Update_Karyawan', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err) => {
        if (err) {
            res.sendStatus(403);
        }
        else {

            let emp = req.body;
            var sql = "SET @update_by = ?; SET @idKaryawan = ?; SET @namaKaryawan = ?; SET @email = ?; SET @foto = ?; SET @Tanggal_Lahir = ?;\
            SET @Alamat = ?; SET @Tempat_Lahir = ?; SET @NPWP = ?; SET @BPJS_TenagaKerja = ?; SET @BPJS_Kesehatan = ?;\
            SET @Id_Jabatan = ?; SET @Alamat_Kerja = ?; SET @Telp_Kerja = ?;\
                CALL bts.UpdateKaryawan(@update_by, @idKaryawan, @namaKaryawan, @email, @foto, @Tanggal_Lahir, @Alamat, @Tempat_Lahir, @NPWP, @BPJS_TenagaKerja,\
                    @BPJS_Kesehatan , @Id_Jabatan , @Alamat_Kerja , @Telp_Kerja);";
            mysqlConnection.query(sql, [emp.Update_by, emp.Id_Karyawan, emp.Nama_Karyawan, emp.Email, emp.Foto, emp.Tanggal_Lahir, emp.Alamat, emp.Tempat_Lahir, emp.NPWP, emp.BPJS_TenagaKerja,
            emp.BPJS_Kesehatan, emp.Id_Jabatan, emp.Alamat_Kerja, emp.Telp_Kerja], (err, rows, fields) => {
                if (!err)
                    rows.forEach(element => {
                        if (element.constructor == Array)
                            res.send("Status Update : " + element[0].statsu + ". \n Data yang diubah : " + element[0].keteranganUpdate.substring(0, element[0].keteranganUpdate.length - 2) + ".");
                    });
                else console.log(err);
            });
        }
    });
});

//get Karyawan By NIP
app.get('/Get_Karyawan_By_NIP/:Id_Karyawan', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err) => {
        if (err) {
            res.sendStatus(403);
        }
        else {
            let emp = req.params;

            var sql = 'select * from karyawan where NIP like ?';

            mysqlConnection.query(sql, [emp.Id_karyawan], (err, rows, fields) => {
                if (!err) {
                    res.send(rows);
                }
                else console.log(err);
            });
        }
    });
});

// delete (karyawan,user,rekening,evaluasi,projek_karyawan)
app.put('/Delete_Karyawan', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err) => {
        if (err) {
            res.sendStatus(403);
        }
        else {
            let emp = req.body;

            var sql = 'set @NIP = ?; set @Create_by = ?;\
            call deleteKaryawan(@NIP,@Create_by);';

            mysqlConnection.query(sql, [emp.NIP, emp.Create_by], (err, rows, fields) => {
                if (!err) {
                    res.send(rows);
                }
                else console.log(err);
            });
        }
    });
});

app.post('/Insert_User', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err) => {
        if (err) {
            res.sendStatus(403);
        }
        else {
            let emp = req.body;

            var sql = 'set @email = ?; set @Create_by = ?;\
            call insertUser(@email,@Create_by);';

            mysqlConnection.query(sql, [emp.Email, emp.Create_by], (err, rows, fields) => {
                if (!err) {
                    res.send(rows);
                }
                else console.log(err);
            });
        }
    });
});

app.get('/Get_All_User', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err) => {
        if (err) {
            res.sendStatus(403);
        }
        else {
            var sql = 'select * from user';

            mysqlConnection.query(sql, (err, rows, fields) => {
                if (!err) {
                    res.send(rows);
                }
                else console.log(err);
            });
        }
    });
});

app.get('/Get_User_By_Id/:Id_User', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err) => {
        if (err) {
            res.sendStatus(403);
        }
        else {
            let emp = req.params;

            var sql = 'select * from user where Id_User = ?';

            mysqlConnection.query(sql, [emp.Id_User], (err, rows, fields) => {
                if (!err) {
                    res.send(rows);
                }
                else console.log(err);
            });
        }
    });
});

app.put('/Delete_User', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err) => {
        if (err) {
            res.sendStatus(403);
        }
        else {
            let emp = req.body;

            var sql = 'set @Id_User = ?; set @Create_by = ?;\
            call deleteUser(@Id_User,@Create_by);';

            mysqlConnection.query(sql, [emp.Id_User, emp.Create_by], (err, rows, fields) => {
                if (!err) {
                    res.send(rows);
                }
                else console.log(err);
            });
        }
    });
});

app.put('/Update_User', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err) => {
        if (err) {
            res.sendStatus(403);
        }
        else {
            let emp = req.body;

            var sql = 'set @Id_User = ?; set @Password = ?; set @Create_by = ?;\
            call updateUser(@Id_User,@Password,@Create_by);';

            mysqlConnection.query(sql, [emp.Id_User, emp.Password, emp.Create_by], (err, rows, fields) => {
                if (!err) {
                    res.send(rows);
                }
                else console.log(err);
            });
        }
    });
});

app.post('/Insert_Departemen', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err) => {
        if (err) {
            res.sendStatus(403);
        }
        else {
            let emp = req.body;

            var sql = 'set @Nama_Departemen = ?; set @Create_by = ?; set @Id_Divisi = ?;\
            call insertDepartemen(@Nama_Departemen,@Create_by,@Id_Divisi);';

            mysqlConnection.query(sql, [emp.Nama_Departemen, emp.Create_by, emp.Id_Divisi], (err, rows, fields) => {
                if (!err) {
                    res.send(rows);
                }
                else console.log(err);
            });
        }
    });
});

app.get('/Get_All_Departemen', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err) => {
        if (err) {
            res.sendStatus(403);
        }
        else {
            var sql = 'select * from departemen;';
            mysqlConnection.query(sql, (err, rows, fields) => {
                if (!err) {
                    res.send(rows);
                }
                else console.log(err);
            });
        }
    });
});

app.get('/Get_Departemen_By_Id/:Id_Departemen', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err) => {
        if (err) {
            res.sendStatus(403);
        }
        else {
            let emp = req.params;

            var sql = 'select * from departemen where Id_Departemen = ?';

            mysqlConnection.query(sql, [emp.Id_Departemen], (err, rows, fields) => {
                if (!err) {
                    res.send(rows);
                }
                else console.log(err);
            });
        }
    });
});

app.put('/Delete_Departemen', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err) => {
        if (err) {
            res.sendStatus(403);
        }
        else {
            let emp = req.body;

            var sql = 'set @Id_Departemen = ?;set @Create_by = ?;\
            call deleteDepartemen(@Id_Departemen,@Create_by);';

            mysqlConnection.query(sql, [emp.Id_Departemen, emp.Create_by], (err, rows, fields) => {
                if (!err) {
                    res.send(rows);
                }
                else console.log(err);
            });
        }
    });
});

app.put('/Update_Departemen', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err) => {
        if (err) {
            res.sendStatus(403);
        }
        else {
            let emp = req.body;

            var sql = 'set @Id_Departemen = ?; set @Nama = ?; set @Create_at = ?;\
            call updateDepartemen(@Id_Departemen,@Nama,@Create_at)';

            mysqlConnection.query(sql, [emp.Id_Departemen, emp.Nama_Departemen, emp.Create_by], (err, rows, fields) => {
                if (!err) {
                    res.send(rows);
                }
                else console.log(err);
            });
        }
    });
});

app.post('/Insert_Proyek', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err) => {
        if (err) {
            res.sendStatus(403);
        }
        else {
            let emp = req.body;

            var sql = 'set @Nama = ?; set @Tanggal_M = ?; set @Tanggal_S = ?; set @Status = ?;\
            set @Klien = ?; set @Kategori = ?; set @Create_by = ?;\
            call insertProyek(@Nama,@Tanggal_M,@Tanggal_S,@Status,@Klien,@Kategori,@Create_by);';

            mysqlConnection.query(sql, [emp.Nama_Proyek, emp.Tanggal_Mulai, emp.Tanggal_Selesai, emp.Status, emp.Klien, emp.Kategori, emp.Create_by], (err, rows, fields) => {
                if (!err) {
                    res.send(rows);
                }
                else console.log(err);
            });
        }
    });
});

app.get('/Get_All_Proyek', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err) => {
        if (err) {
            res.sendStatus(403);
        }
        else {
            var sql = 'select * from proyek;';
            mysqlConnection.query(sql, (err, rows, fields) => {
                if (!err) {
                    res.send(rows);
                }
                else console.log(err);
            });
        }
    });
});

app.get('/Get_Proyek_By_Id/:Id_Proyek', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err) => {
        if (err) {
            res.sendStatus(403);
        }
        else {
            let emp = req.params;

            var sql = 'select * from proyek where Id_Proyek = ?';
            mysqlConnection.query(sql, [emp.Id_Proyek], (err, rows, fields) => {
                if (!err) {
                    res.send(rows);
                }
                else console.log(err);
            });
        }
    });
});

app.put('/Update_Proyek', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err) => {
        if (err) {
            res.sendStatus(403);
        }
        else {
            let emp = req.body;

            var sql = 'set @Id_P = ?; set @Nama = ?; set @Tanggal_S = ?; set @Status = ?; set @Kategori = ?; set @Create_by = ?;\
            call updateProyek(@Id_P,@Nama,@Tanggal_S,@Status,@Kategori,@Create_by);';

            mysqlConnection.query(sql, [emp.Id_Proyek, emp.Nama, emp.Tanggal_S, emp.Status, emp.Kategori, emp.Create_by], (err, rows, fields) => {
                if (!err) {
                    res.send(rows);
                }
                else console.log(err);
            });
        }
    });
});

app.put('/Delete_Proyek', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err) => {
        if (err) {
            res.sendStatus(403);
        }
        else {
            let emp = req.body;

            var sql = 'set @Id_Proyek = ?;set @Create_by = ?;\
            call deleteProyek(@Id_Proyek,@Create_by);';

            mysqlConnection.query(sql, [emp.Id_Proyek, emp.Create_by], (err, rows, fields) => {
                if (!err) {
                    res.send(rows);
                }
                else console.log(err);
            });
        }
    });
});

// insert Divisi
app.post('/Tambah_Divisi', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err) => {
        if (err) {
            res.sendStatus(403);
        }
        else {
            let emp = req.body;
            var sql = "SET @nama_divisi = ?; SET @update_by = ?; \
                CALL InsertDivisi(@nama_divisi, @update_by);";
            mysqlConnection.query(sql, [emp.Nama_Divisi, emp.Create_by], (err, rows, fields) => {
                if (!err)
                    rows.forEach(element => {
                        if (element.constructor == Array)
                            res.send(element[0].kesimpulan/*+"\n Waktu Penambahan : "+element[0].deleteStatus*/);
                    });
                else console.log(err);
            });
        }
    });
});

//Delete DIVISI
app.put('/Delete_Divisi', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err) => {
        if (err) {
            res.sendStatus(403);
        }
        else {
            mysqlConnection.query('UPDATE bts.divisi SET Update_at = curdate(),  Delete_at = curdate() WHERE divisi.Nama_Divisi like ? and Delete_at is null;', [req.body.nama_divisi], (err, rows, fields) => {
                if (!err)
                    var baris = rows.affectedRows;
                if (baris != 0) {
                    res.send('DELETE Divisi SUCCESSFULLY');
                    mysqlConnection.query('call `InsertHistory`(?, "Delete Divisi ? .");', [req.body.Create_by, req.body.Nama_Divisi]);
                }
                else if (baris == 0) res.send('DELETE Divisi FAILED. \n DIVISION name not found OR DIVISION has been DELETED.');
                else console.log(err);
            });
        }
    });
});


//Update DIVISI
app.put('/Update_Divisi', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err) => {
        if (err) {
            res.sendStatus(403);
        }
        else {
            mysqlConnection.query('UPDATE bts.divisi SET Nama_Divisi = ?, Update_at = curdate()  WHERE divisi.Id_Divisi = ? and Delete_at is null;' /* call SP UPDATE DIVISI_DEPARTEMEN*/, [req.body.nama_divisi, req.body.id_divisi], (err, rows, fields) => {
                if (!err)
                    var baris = rows.affectedRows;
                console.log(baris);
                if (baris != 0) {
                    res.send('Update Divisi SUCCESSFULLY');
                    mysqlConnection.query('call `InsertHistory`(?, "Update Divisi lama dengan ID Divisi : ? , dengan nama baru : ? .");', [req.body.Create_by, req.body.Id_Divisi, req.body.Nama_Divisi]);
                }
                else if (baris == 0) res.send('Update Divisi FAILED. \n DIVISION name not found OR THE DIVISION WAS REMOVED.');
                else console.log(err);
            });
        }
    });
});

//Select all DIVISI
app.get('/Get_All_Divisi', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err) => {
        if (err) {
            res.sendStatus(403);
        }
        else {
            mysqlConnection.query('Select * from bts.divisi where Delete_at is null;' /* call SP UPDATE DIVISI_DEPARTEMEN*/, (err, rows, fields) => {
                if (!err) res.send(rows);
                else console.log(err);
            });
        }
    });
});

//Select DIVISI by id
app.get('/Get_Divisi_By_Id/:id', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err) => {
        if (err) {
            res.sendStatus(403);
        }
        else {
            mysqlConnection.query('Select * from bts.divisi where Id_Divisi = ? and Delete_at is null;' /* call SP UPDATE DIVISI_DEPARTEMEN*/, [req.params.id], (err, rows, fields) => {
                if (!err) res.send(rows);
                else console.log(err);
            });
        }
    });
});

app.post('/Insert_Rekening', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err) => {
        if (err) {
            res.sendStatus(403);
        }
        else {
            let emp = req.body;

            var sql = 'set @Nomor = ?;set @NIP = ?; set @Create_by = ?;\
            call insertRekening(@Nomor,@NIP,@Create_by);';

            mysqlConnection.query(sql, [emp.Nomor_Rekening, emp.NIP, emp.Create_by], (err, rows, fields) => {
                if (!err) {
                    res.send(rows);
                }
                else console.log(err);
            });
        }
    });
});

app.get('/Get_All_Rekening', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err) => {
        if (err) {
            res.sendStatus(403);
        }
        else {
            var sql = 'select * from rekening;';

            mysqlConnection.query(sql, (err, rows, fields) => {
                if (!err) {
                    res.send(rows);
                }
                else console.log(err);
            });
        }
    });
});

app.get('/Get_Rekening_By_Id/:Id_Rekening', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err) => {
        if (err) {
            res.sendStatus(403);
        }
        else {
            let emp = req.params;
            var sql = 'select * from rekening where Id_Rekening = ?;';

            mysqlConnection.query(sql, [emp.Id_Rekening], (err, rows, fields) => {
                if (!err) {
                    res.send(rows);
                }
                else console.log(err);
            });
        }
    });
});

app.put('/Update_Rekening', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err) => {
        if (err) {
            res.sendStatus(403);
        }
        else {
            let emp = req.body;

            var sql = 'set @Id_R = ?; set @Nomor = ?;set @Create_by = ?;\
            call updateRekening(@Id_R,@Nomor,@Create_by);';

            mysqlConnection.query(sql, [emp.Id_Rekening, emp.Nomor_Rekening, emp.Create_by], (err, rows, fields) => {
                if (!err) {
                    res.send(rows);
                }
                else console.log(err);
            });
        }
    });
});

app.put('/Delete_Rekening', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err) => {
        if (err) {
            res.sendStatus(403);
        }
        else {
            let emp = req.body;

            var sql = 'set @Id_R = ?; set @Create_by = ?;\
            call deleteRekening(@Id_R,@Create_by);';

            mysqlConnection.query(sql, [emp.Id_Rekening, emp.Create_by], (err, rows, fields) => {
                if (!err) {
                    res.send(rows);
                }
                else console.log(err);
            });
        }
    });
});

app.get('/Get_All_Log', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err) => {
        if (err) {
            res.sendStatus(403);
        }
        else {
            var sql = 'select * from log;';

            mysqlConnection.query(sql, (err, rows, fields) => {
                if (!err) {
                    res.send(rows);
                }
                else console.log(err);
            });
        }
    });
});

app.get('/Get_Log_By_Id_Create_by/:Id_Log', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err) => {
        if (err) {
            res.sendStatus(403);
        }
        else {
            let emp = req.params;

            var sql = 'select * from log where Create_by = ?;';

            mysqlConnection.query(sql, [emp.Id_log], (err, rows, fields) => {
                if (!err) {
                    res.send(rows);
                }
                else console.log(err);
            });
        }
    });
});
// insert Jabatan
app.post('/Tambah_Jabatan', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err) => {
        if (err) {
            res.sendStatus(403);
        }
        else {
            let emp = req.body;
            var sql = "SET @Nama_Jabatan = ?; SET @update_by = ?; SET @id_Departemen = ?; SET @level = ?; \
                CALL InsertJabatan(@Nama_Jabatan, @update_by, @id_Departemen, @level);";
            mysqlConnection.query(sql, [emp.Nama_Jabatan, emp.Create_by, emp.Id_Departemen, emp.Level], (err, rows, fields) => {
                if (!err)
                    rows.forEach(element => {
                        if (element.constructor == Array)
                            res.send(element[0].kesimpulan/*+"\n Waktu Penambahan : "+element[0].deleteStatus*/);
                    });
                else console.log(err);
            });
        }
    });
});

//Update Jabatan
app.put('/Update_Jabatan', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err) => {
        if (err) {
            res.sendStatus(403);
        }
        else {
            mysqlConnection.query('SET @Nama_Jabatan_Lama = ?; SET @Nama_Jabatan_Baru = ?; SET @update_by = ?; SET @id_Departemen = ?; SET @level = ?; \
                CALL UpdateJabatan(@Nama_Jabatan_Lama, @Nama_Jabatan_Baru, @update_by, @id_Departemen, @level);' /* call SP UPDATE DIVISI_DEPARTEMEN*/, [req.body.Nama_Jabatan_Lama, req.body.Nama_Jabatan_Baru,
                req.body.Create_by, req.body.Id_Departemen, req.body.Level], (err, rows, fields) => {
                    if (!err)
                        rows.forEach(element => {
                            if (element.constructor == Array)
                                res.send(element[0].kesimpulan);
                        });
                    else console.log(err);
                });
        }
    });
});

//Select all Jabatan
app.get('/Get_All_Jabatan', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err) => {
        if (err) {
            res.sendStatus(403);
        }
        else {
            mysqlConnection.query('Select * from bts.jabatan where Delete_at is null;', (err, rows, fields) => {
                if (!err) res.send(rows);
                else console.log(err);
            });
        }
    });
});

//Select JABATAN by id jabatan
app.get('/Get_Jabatan_By_IdJabatan/:Id_Jabatan', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err) => {
        if (err) {
            res.sendStatus(403);
        }
        else {
            mysqlConnection.query('Select * from bts.jabatan where Id_Jabatan = ? and Delete_at is null;', [req.params.Id_Jabatan], (err, rows, fields) => {
                if (!err) res.send(rows);
                else console.log(err);
            });
        }
    });
});

//Select JABATAN by id Departemen
app.get('/Get_Jabatan_By_IdDepartemen/:Id_Departemen', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err) => {
        if (err) {
            res.sendStatus(403);
        }
        else {
            mysqlConnection.query('Select * from bts.jabatan where Id_Departemen = ? and Delete_at is null;', [req.params.Id_Departemen], (err, rows, fields) => {
                if (!err) res.send(rows);
                else console.log(err);
            });
        }
    });
});

//Delete Jabatan
app.put('/Delete_Jabatan', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err) => {
        if (err) {
            res.sendStatus(403);
        }
        else {
            mysqlConnection.query('UPDATE bts.jabatan SET Update_at = curdate(),  Delete_at = curdate() WHERE jabatan.Nama_Jabatan like ? and Delete_at is null;', [req.body.nama_jabatan], (err, rows, fields) => {
                if (!err)
                    var baris = rows.affectedRows;
                if (baris != 0) {
                    res.send('DELETE Jabatan SUCCESSFULLY');
                    mysqlConnection.query('call `InsertHistory`(?, "Delete Jabatan ? .");', [req.body.Create_by, req.body.Nama_Jabatan]);
                }
                else if (baris == 0) res.send('DELETE Jabatan FAILED. \n Jabatan name not found OR DIVISION has been DELETED.');
                else console.log(err);
            });
        }
    });
});

app.post('/Insert_Status_Karyawan', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err) => {
        if (err) {
            res.sendStatus(403);
        }
        else {
            let emp = req.body;

            var sql = 'set @nama = ?; set @Create_by = ?;\
            call insertStatusKaryawan(@nama,@Create_by);';

            mysqlConnection.query(sql, [emp.Nama_Status, emp.Create_by], (err, rows, fields) => {
                if (!err) {
                    res.send(rows);
                }
                else console.log(err);
            });
        }
    });
});

app.get('/Get_All_Status_Karyawan', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err) => {
        if (err) {
            res.sendStatus(403);
        }
        else {
            var sql = 'select * from status_karyawan;';

            mysqlConnection.query(sql, (err, rows, fields) => {
                if (!err) {
                    res.send(rows);
                }
                else console.log(err);
            });
        }
    });
});

app.get('/Get_Status_Karyawan_By_Id/:Id', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err) => {
        if (err) {
            res.sendStatus(403);
        }
        else {
            let emp = req.params;

            var sql = 'select * from status_karyawan where Id_Status = ?;';

            mysqlConnection.query(sql, [emp.Id], (err, rows, fields) => {
                if (!err) {
                    res.send(rows);
                }
                else console.log(err);
            });
        }
    });
});

app.put('/Update_Status_karyawan', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err) => {
        if (err) {
            res.sendStatus(403);
        }
        else {
            let emp = req.body;

            var sql = 'set @Id_S = ?; set @Nama = ?; set @Create_by = ?;\
            call updateStatusKaryawan(@Id_S,@Nama,@Create_by)';

            mysqlConnection.query(sql, [emp.Id_Status, emp.Nama_Status, emp.Create_by], (err, rows, fields) => {
                if (!err) {
                    res.send(rows);
                }
                else console.log(err);
            });
        }
    });
});

app.put('/Delete_Status_Karyawan', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err) => {
        if (err) {
            res.sendStatus(403);
        }
        else {
            let emp = req.body;

            var sql = 'set @Id_S = ?; set @Create_by = ?;\
        call deleteStatusKaryawan(@Id_S,@Create_by);'

            mysqlConnection.query(sql, [emp.Id_Status, emp.Create_by], (err, rows, fields) => {
                if (!err) {
                    res.send(rows);
                }
                else console.log(err);
            });
        }
    });
});

app.post('/Insert_Todo_List', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err) => {
        if (err) {
            res.sendStatus(403);
        }
        else {
            let emp = req.body;

            var sql = 'set @Nama = ?; set @Desk = ?; set @Status = ?; set @Create_by = ?;\
            call insertTodoList(@Nama,@Desk,@Status,@Create_by);';

            mysqlConnection.query(sql, [emp.Nama_Task, emp.Deskripsi, emp.Status_Task, emp.Create_by], (err, rows, fields) => {
                if (!err) {
                    res.send(rows);
                }
                else console.log(err);
            });
        }
    });
});

app.get('/Get_All_Todo_List', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err) => {
        if (err) {
            res.sendStatus(403);
        }
        else {
            var sql = 'select * from todo_list';

            mysqlConnection.query(sql, (err, rows, fields) => {
                if (!err) {
                    res.send(rows);
                }
                else console.log(err);
            });
        }
    });
});

app.get('/Get_Todo_List_By_Id/:Id', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err) => {
        if (err) {
            res.sendStatus(403);
        }
        else {
            let emp = req.params;

            var sql = 'select * from todo_list where Id_Task = ?';

            mysqlConnection.query(sql, [emp.Id], (err, rows, fields) => {
                if (!err) {
                    res.send(rows);
                }
                else console.log(err);
            });
        }
    });
});

app.put('/Update_Todo_List', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err) => {
        if (err) {
            res.sendStatus(403);
        }
        else {
            let emp = req.body;

            var sql = 'set @Id_T = ?; set @Nama = ?; set @Desk = ?; set @Status_Task = ?; set @Id_User = ?; set @Create_by = ?;\
            call updateTodoList(@Id_T,@Nama,@Desk,@Status_Task,@Id_User,@Create_by);';

            mysqlConnection.query(sql, [emp.Id_Task, emp.Nama_Task, emp.Deskripsi, emp.Status_Task, emp.Id_User, emp.Create_by], (err, rows, fields) => {
                if (!err) {
                    res.send(rows);
                }
                else console.log(err);
            });
        }
    });
});

app.put('/Delete_Todo_List', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err) => {
        if (err) {
            res.sendStatus(403);
        }
        else {
            let emp = req.body;

            var sql = 'set @Id_T = ?; set @Create_by = ?;\
            call deleteTodoList(@Id_T,@Create_by);';

            mysqlConnection.query(sql, [emp.Id_Task, emp.Create_by], (err, rows, fields) => {
                if (!err) {
                    res.send(rows);
                }
                else console.log(err);
            });
        }
    });
});
//Select all Proyek_Karyawan
app.get('/Get_All_Proyek_Karyawan', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err) => {
        if (err) {
            res.sendStatus(403);
        }
        else {
            mysqlConnection.query('Select * from bts.proyek_karyawan where Delete_at is null;', (err, rows, fields) => {
                if (!err) res.send(rows);
                else console.log(err);
            });
        }
    });
});

//Select Proyek_Karyawan by Id_Proyek_Karyawan
app.get('/Get_Proyek_Karyawan_By_Id_ProyekKaryawan/:id', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err) => {
        if (err) {
            res.sendStatus(403);
        }
        else {
            mysqlConnection.query('Select * from bts.proyek_karyawan where Id_Proyek_Karyawan = ? and Delete_at is null;', [req.params.id], (err, rows, fields) => {
                if (!err) res.send(rows);
                else console.log(err);
            });
        }
    });
});

//Select Proyek_Karyawan by Id_Proyek_Karyawan
app.get('/Get_Proyek_Karyawan_By_Id_Proyek/:id', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err) => {
        if (err) {
            res.sendStatus(403);
        }
        else {
            mysqlConnection.query('Select * from bts.proyek_karyawan where Id_Proyek = ? and Delete_at is null;', [req.params.id], (err, rows, fields) => {
                if (!err) res.send(rows);
                else console.log(err);
            });
        }
    });
});

//Select Proyek_Karyawan by Id_Karyawan
app.get('/Get_Proyek_Karyawan_By_Id_Karyawan/:id', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err) => {
        if (err) {
            res.sendStatus(403);
        }
        else {
            mysqlConnection.query('Select * from bts.proyek_karyawan where Id_Karyawan = ? and Delete_at is null;', [req.params.id], (err, rows, fields) => {
                if (!err) res.send(rows);
                else console.log(err);
            });
        }
    });
});

// insert project_karyawan
app.post('/Tambah_Proyek_Karyawan', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err) => {
        if (err) {
            res.sendStatus(403);
        }
        else {
            let emp = req.body;
            var sql = "SET @Id_Karyawan = ?; SET @Id_Proyek = ?; SET @Id_Creat_by = ?; \
                CALL InsertProyekKaryawan(@Id_Karyawan, @Id_Proyek, @Id_Creat_by);";
            mysqlConnection.query(sql, [emp.Id_Karyawan, emp.Id_Proyek, emp.Id_Creat_by], (err, rows, fields) => {
                if (!err)
                    rows.forEach(element => {
                        if (element.constructor == Array)
                            res.send(element[0].kesimpulan/*+"\n Waktu Penambahan : "+element[0].deleteStatus*/);
                    });
                else console.log(err);
            });
        }
    });
});


//Select all Evaluasi
app.get('/Get_All_Evaluasi', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err) => {
        if (err) {
            res.sendStatus(403);
        }
        else {
            mysqlConnection.query('Select * from bts.evaluasi where Delete_at is null;', (err, rows, fields) => {
                if (!err) res.send(rows);
                else console.log(err);
            });
        }
    });
});

//Select Evaluasi by id Evaluasi
app.get('/Get_Evaluasi_By_Id_Evaluasi/:id'/*, verifyToken*/, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err) => {
        if (err) {
            res.sendStatus(403);
        }
        else {
            mysqlConnection.query('Select * from bts.evaluasi where Id_Evaluasi = ? and Delete_at is null;', [req.params.id], (err, rows, fields) => {
                if (!err) res.send(rows);
                else console.log(err);
            });
        }
    });
});

//Select Evaluasi by idKaryawan
app.get('/Get_Evaluasi_By_Id_Karyawan/:id', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err) => {
        if (err) {
            res.sendStatus(403);
        }
        else {
            mysqlConnection.query('Select * from bts.evaluasi where NIP = ? and Delete_at is null;', [req.params.id], (err, rows, fields) => {
                if (!err) res.send(rows);
                else console.log(err);
            });
        }
    });
});

//Insert Evaluasi (in_progress)
app.post('/Insert_Evaluasi', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err) => {
        if (err) {
            res.sendStatus(403);
        }
        else {

            let emp = req.body;
            //id_nip varchar(20), id_user int, id_Stat int(11), Tanggal_Msk date, masaKerja date, Tanggal_Eval date, Tgl_Resign date
            var sql = "SET @update_by = ?; SET @idKaryawan = ?; SET @idStatus = ?; SET @tanggal_masuk = ?; SET @masa_kerja = ?; SET @Tanggal_Evaluasi = ?;\
            SET @Tanggal_Resign = ?;\
                CALL bts.InsertEvaluasi(@idKaryawan,@update_by, @idStatus, @tanggal_masuk, @masa_kerja, @Tanggal_Evaluasi, @Tanggal_Resign);";
            mysqlConnection.query(sql, [emp.Id_Karyawan, emp.Update_by, emp.Id_Status, emp.Tanggal_Masuk, emp.Masa_Kerja, emp.Tanggal_Evaluasi, emp.Tanggal_Resign], (err, rows, fields) => {
                if (!err)
                    rows.forEach(element => {
                        if (element.constructor == Array)
                            res.send(element[0].kesimpulan + ". \n  ");//element[0].keteranganUpdate.substring(0,element[0].keteranganUpdate.length-2)+".");


                    });
                else console.log(err);
            });
        }
    });
});

//Update Evaluasi
app.put('/Update_Evaluasi', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err) => {
        if (err) {
            res.sendStatus(403);
        }
        else {
            let emp = req.body;
            //id_nip varchar(20), id_user int, id_Stat int(11), Tanggal_Msk date, masaKerja date, Tanggal_Eval date, Tgl_Resign date
            var sql = "SET @update_by = ?; SET @idKaryawan = ?; SET @idStatus = ?; SET @tanggal_masuk = ?; SET @masa_kerja = ?; SET @Tanggal_Evaluasi = ?;\
            SET @Tanggal_Resign = ?;\
                CALL bts.InsertEvaluasi(@update_by, @idKaryawan, @idStatus, @tanggal_masuk, @masa_kerja, @Tanggal_Evaluasi, @Tanggal_Resign);";
            mysqlConnection.query(sql, [emp.Update_by, emp.Id_Karyawan, emp.Id_Status, emp.Tanggal_Masuk, emp.Masa_Kerja, emp.Tanggal_Evaluasi, emp.Tanggal_Resign], (err, rows, fields) => {
                if (!err)
                    rows.forEach(element => {
                        if (element.constructor == Array)
                            res.send(element[0].kesimpulan + ". \n  " + element[0].keteranganUpdate.substring(0, element[0].keteranganUpdate.length - 2) + ".");
                    });
                else console.log(err);
            });
        }
    });
});