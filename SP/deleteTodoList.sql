CREATE DEFINER=`root`@`localhost` PROCEDURE `deleteTodoList`(
Id_T int,
Create_by int)
BEGIN
	declare count int;
    
    select count(Id_Task) into count from todo_list where Id_Task = Id_T and Delete_at is null;
    
    if count = 1
    then
		update todo_list set Delete_at = curdate() where Id_Task = Id_T;
        
        insert into log(Keterangan,Create_at,Create_by) values(concat('Delete Todo List dengan Id_Task : ',Id_T),curdate(),Create_by);
    end if;
END