CREATE DEFINER=`root`@`localhost` PROCEDURE `insertUser`(
email_ varchar(100),
Create_by int)
BEGIN
	declare count,id_u int;
    select count(Id_User) into count from user where email like email_ and (Delete_at is not null or Delete_at is null); -- 1 sudah ada, 0 belum ada delete_at null 
    
	if count=0
    then
		insert into user (email,Password,Role,Create_at) values(email_,'password',0,curdate());
        
        select Id_User into id_u from user where email like email_;
        insert into log(Keterangan,Create_at,Create_by) values(concat("Menambahkan admin dengan Id_User : ",id_u),curdate(),Create_by);
	else if count =1
    then
		-- asumsi email -> delete_at = null
		update user set Create_at = curdate(), Update_at=curdate(), delete_at = null and password = "password" where email like email_;
        
        select Id_User into id_u from user where email like email_;
        insert into log(Keterangan,Create_at,Create_by,Id_User) values(concat("Merubah delete_at menjadi null dan merubah password menjadi default dengan Id_User : ",id_u),curdate(),Create_by,id_u);
    end if;
    end if;
    select count;
END