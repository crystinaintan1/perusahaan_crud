CREATE DEFINER=`root`@`localhost` PROCEDURE `InsertJabatan`(nama_jab varchar(50), id_User int(11), id_Departemen int(11), lev int(11))
BEGIN
	DECLARE cekNamaJabatan varchar(50);
    DECLARE namaBaru varchar(50);
    DECLARE IdJabatan int(11);
    DECLARE update_by int(11);
    DECLARE IdDepart int(11);
    DECLARE low int(11);
    DECLARE eksisDepartemen int(11);
    DECLARE deleteStatus date;
    DECLARE hasil varchar(1000);
    
    SELECT nama_jab INTO namaBaru;
    SELECT Nama_Jabatan from jabatan where jabatan.Nama_Jabatan like namaBaru into cekNamaJabatan;
    SELECT Id_Jabatan from jabatan where jabatan.Nama_Jabatan like namaBaru into IdJabatan;
    SELECT id_User into update_by;
    SELECT id_Departemen INTO IdDepart;
	SELECT lev INTO low;
    SELECT Id_Departemen from departemen where departemen.Id_Departemen = IdDepart into eksisDepartemen;
    
    if cekNamaJabatan NOT like null or cekNamaJabatan NOT like ""
    THEN
		SELECT Delete_at from jabatan where jabatan.Nama_Jabatan like namaBaru into deleteStatus;
        IF deleteStatus is not null
        THEN
			IF eksisDepartemen is not null
            THEN
				UPDATE jabatan 
				SET	Delete_at = NULL, Create_at = curdate(), Update_at = curdate(), Id_Departemen = IdDepart, Level = low
                WHERE jabatan.Nama_Jabatan like namaBaru;
                select "Berhasil menambah jabatan lama." as kesimpulan, deleteStatus;
                select concat("Tambah jabatan lama dengan ID Jabatan :", IdJabatan , " dengan nama baru : " , namaBaru ,".") INTO HASIL;
                -- insert history --
				call `InsertHistory`(update_by, hasil);
                -- call SP Insert Divisi_departemen -- 
            ELSE
				select "Gagal menambah Jabatan. Karena Id Departemen salah (tidak ada)." as kesimpulan;
            END IF;
		ELSE
                select "Jabatan sudah ada." as kesimpulan,deleteStatus;
		END IF;
    else
        IF eksisDepartemen is not null
		THEN
			INSERT into bts.jabatan(Nama_Jabatan, Create_at, Id_Departemen, Level) VALUES 
			(namaBaru, curdate(),IdDepart, low);
			select "Berhasil menambah jabatan baru" as kesimpulan;
			select concat("Tambah Jabatan baru dengan nama : " , namaBaru ,".") INTO HASIL;
			-- insert history --
			call `InsertHistory`(update_by, hasil);
			-- call SP update Divisi_departemen -- 
        ELSE
			select "Gagal menambah Jabatan. Karena Id Departemen salah (tidak ada)." as kesimpulan;
        END IF;
	END IF;
END