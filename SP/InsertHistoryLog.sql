CREATE DEFINER=`root`@`localhost` PROCEDURE `InsertHistory`(created_by int(11), ket varchar(1000))
BEGIN
	INSERT into bts.log (Keterangan, Create_at, Create_by) VALUES 
    (ket, curdate(), created_by);
END