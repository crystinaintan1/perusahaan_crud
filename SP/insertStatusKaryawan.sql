CREATE DEFINER=`root`@`localhost` PROCEDURE `insertStatusKaryawan`(
Nama varchar(50),
Create_by int)
BEGIN
	declare count,id_s int;
    
    select count(Id_Status) into count from status_karyawan where Nama_Status like Nama and (Delete_at is not null or Delete_at is null);
    -- 0 belum ada, 1 udah ada delete_at =  null
    
    if count = 0
    then
		insert into status_karyawan (Nama_Status,Create_at) values(Nama,curdate());
        
        select Id_Status into id_s from status_karyawan where Nama_Status like Nama;
        insert into log(Keterangan,Create_at,Create_by) values(concat('Menambahkan Status Karyawan dengan Id_Status : ',id_s),curdate(),Create_by);
    else if count = 1
    then
		update status_karyawan set Create_at = curdate(), Update_at = curdate(), Delete_at = null where Nama_Status like Nama;
        
        select Id_Status into id_s from status_karyawan where Nama_Status like Nama;
        insert into log(Keterangan,Create_at,Create_by) values(concat('Update Status Karyawan dengan Id_Status : ',id_s),curdate(),Create_by);
    end if;
    end if;
    
    select count;
END