CREATE DEFINER=`root`@`localhost` PROCEDURE `insertKaryawan`(NIP varchar(20),
Nama varchar(300),
NIK varchar(30),
email varchar(100), 
Tempat_Lahir varchar(50), 
Tanggal_Lahir date, 
Alamat varchar(250), 
NPWP varchar(200),
BPJS_TenagaKerja varchar(250), 
BPJS_Kesehatan varchar(250) ,
Create_at date ,
Foto longtext ,
Id_Jabatan int(11) ,
Alamat_Kerja varchar(250) ,
Telp_Kerja varchar(200),
Create_by int)
BEGIN
	declare count int;-- 0 berlum ada, 1 ada
    declare id int;
    
    -- cek apakah ada karyawan dengan nip seperti param, 0 belum ada, 1 sudah ada delete_at = null
    select count(nip) into count from karyawan where karyawan.nip like nip and Delete_at is not null;
        
	if count = 0
    then
		
		-- aman
		-- insert karyawan
		insert into karyawan (NIP, Nama, NIK, email, Tempat_Lahir , Tanggal_Lahir,  Alamat, NPWP, BPJS_TenagaKerja, BPJS_Kesehatan,
		Create_at, Foto, Id_Jabatan, Alamat_Kerja, Telp_Kerja) 
        values (NIP, Nama, NIK, email, Tempat_Lahir , Tanggal_Lahir,  Alamat, NPWP, BPJS_TenagaKerja, BPJS_Kesehatan,
		Create_at , Foto, Id_Jabatan ,Alamat_Kerja ,Telp_Kerja);
		-- insert log karyawan
        insert into log(Keterangan,Create_at,Create_by) values (concat('Memasukkan data karyawan dengan NIP : ',NIP),curdate(),create_by);
        
        -- insert user
        insert into user (email,password,Role,Create_at,Id_Karyawan) values (email,'password',1,curdate(),NIP);
        
        select User.Id_User into id from User Where User.email like email;
        
        -- insert log user
        insert into log(Keterangan,Create_at,Create_by) values (concat('Memasukkan data User dengan Id_User : ',id),curdate(),Create_by);
	else
		-- update karyawan
		update karyawan  set Create_at = curdate(), Update_at=curdate(), Delete_at = null where NIP like NIP;
        update user set Create_at = curdate(), Update_at=curdate(), Delete_at = null where NIP like NIP;
        -- log karyawan
        insert into log(Keterangan,Create_at,Create_by) values(concat('Update data karyawan dengan NIP : ',NIP),curdate(),Create_by);
        
        -- update user
        update user set Delete_at = null,password = 'password' where NIP like NIP;
        -- log user
        select User.Id_User into id from User Where User.email like email;
        insert into log(Keterangan,Create_at,Create_by) values(concat('Update data user dengan NIP : ',id),curdate(),Create_by);
        
    end if;
	if count = 0
    then
		select 'Berhasil menambahkan data Karyawan';
	else
		select 'Berhasil memperbaharui data karyawan';
    end if;
END