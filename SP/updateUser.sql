CREATE DEFINER=`root`@`localhost` PROCEDURE `updateUser`(
Id_U int,
Password_ varchar(50),
Create_by int)
BEGIN
    declare Id_K varchar(20);
    declare count int;
    
    select count(Id_User) into count from user where Id_User=Id_U and Delete_at is null;-- 1 ada, 0 ga ada
    
    if count = 1
    then
		update user set Password = Password_ where Id_User = Id_U;
		
		insert into log (Keterangan,Create_at,Create_By) values (concat('Update data user Password dengan Id_User',id_U),curdate(),Create_by);
	end if;
END