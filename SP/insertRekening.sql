CREATE DEFINER=`root`@`localhost` PROCEDURE `insertRekening`(
Nomor int,
NIP varchar(20),
Create_by int)
BEGIN
	declare count,id_r int;
    
    select count(Id_Rekening) into count from rekening where Nomor_Rekening = Nomor and (Delete_at is not null or Delete_at is null);
    -- 0 belum ada, 1 udah ada delete_at =  null
    -- asumsi nomor rekening untuk karywan selalu baru, walaupun karyawan tsb sudah pernah terdaftar (keluar kantor -> masuk lagi)
    if count = 0
    then
		insert into rekening(Nomor_Rekening,Create_at,NIP) values(Nomor,curdate(),NIP);
        
        select Id_Rekening into id_r from rekening where Nomor_Rekening = Nomor;
        insert into log(Keterangan,Create_at,Create_by) values(concat('Menambahkan nomor rekening dengan Id_Rekening : ',id_r),curdate(),Create_by);
    else if count = 1
    then
		update rekening set Create_at = curdate(), Update_at=curdate(), delete_at = null where Nomor_Rekening = Nomor;
        
        select Id_Rekening into id_r from rekening where Nomor_Rekening = Nomor;
        insert into log (Keterangan,Create_at,Create_by) values(concat('Update rekening (delete_at) dengan Id_Rekening : ',id_r),curdate(),Create_by);
    end if;
    end if;
    
    select count;
END