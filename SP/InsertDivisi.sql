CREATE DEFINER=`root`@`localhost` PROCEDURE `InsertDivisi`(nama_div varchar(50), id_User int(11))
BEGIN
	DECLARE cekNamaDiv varchar(50);
    DECLARE namaBaru varchar(50);
    DECLARE IdDivisi int(11);
    DECLARE update_by int(11);
    DECLARE deleteStatus date;
    DECLARE hasil varchar(1000);
    
    SELECT nama_div INTO namaBaru;
    SELECT Nama_Divisi from divisi where divisi.Nama_Divisi like namaBaru into cekNamaDiv;
    SELECT Id_Divisi from divisi where divisi.Nama_Divisi like namaBaru into IdDivisi;
    SELECT id_User into update_by;
    if cekNamaDiv NOT like null or cekNamaDiv NOT like ""
    THEN
		SELECT Delete_at from divisi where divisi.Nama_Divisi like namaBaru into deleteStatus;
        IF deleteStatus is not null
        THEN
			UPDATE divisi 
				SET	Delete_at = NULL, Create_at = curdate(), Update_at = curdate()
                WHERE divisi.Nama_Divisi like namaBaru;
                select "Berhasil menambah divisi lama." as kesimpulan, deleteStatus;
                select concat("Tambah Divisi lama dengan ID Divisi :", IdDivisi , " dengan nama baru : " , namaBaru ,".") INTO HASIL;
                -- insert history --
				call `InsertHistory`(update_by, hasil);
                -- call SP Insert Divisi_departemen -- 
		ELSE
                select "Divisi sudah ada." as kesimpulan,deleteStatus;
		END IF;
    else
        INSERT into bts.divisi(Nama_Divisi, Create_at) VALUES 
		(namaBaru, curdate());
		select "Berhasil menambah divisi baru" as kesimpulan;
        select concat("Tambah Divisi baru dengan nama : " , namaBaru ,".") INTO HASIL;
		-- insert history --
		call `InsertHistory`(update_by, hasil);
        -- call SP update Divisi_departemen -- 
        END IF;
END