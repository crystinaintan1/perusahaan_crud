CREATE DEFINER=`root`@`localhost` PROCEDURE `UpdateKaryawan`(update_by int, idChange varchar(20), namaKaryawan varchar(300), email varchar(100),
 foto longtext, Tanggal_Lahir date, address varchar(250),
Tempat_Lahir varchar(50), NPWP varchar(200), BPJS_TenagaKerja varchar(250), BPJS_Kesehatan varchar(250),
Id_Div INT(11), Id_Depart INT(11), Id_Jab INT(11), Alamat_Kerja varchar(250), Telp_Kerja varchar(200))
BEGIN
	DECLARE findKaryawan varchar(20);
    DECLARE findKaryawanAktive varchar(20);
    DECLARE keteranganUpdate varchar(200);
    DECLARE name_divisi varchar(50);
    DECLARE name_departemen varchar(50);
    DECLARE name_jabatan varchar(50);
    DECLARE hasil varchar(1000);
    
    
    select karyawan.NIP into findKaryawan from karyawan where karyawan.NIP like idChange;
    select "" into keteranganUpdate;
    -- tidak ada karyawan dengan nip tersebut--
    if findKaryawan = null
    THEN
		Select "Belum ada Karyawan dengan NIP : " + idChange;
	else
		 select karyawan.NIP into findKaryawanAktive from karyawan where bts.karyawan.NIP like idChange and bts.karyawan.Delete_at is null;
        -- ada karyawan dengan nip tsb dan masih aktive--
		if findKaryawanAktive is not null
        THEN
			if namaKaryawan IS NOT NULL
			THEN UPDATE karyawan
				SET Nama = namaKaryawan ,Update_at = curdate()
				WHERE karyawan.NIP like idChange;
			
            select concat(keteranganUpdate, "Nama Karyawan , ") INTO keteranganUpdate;
            select concat("Update nama karyawan lama dengan NIP :", idChange , " dengan nama baru : " , namaKaryawan ,".") INTO HASIL;
            -- insert history --
            call `InsertHistory`(update_by, hasil);
			END IF;
			
			if email IS NOT NULL
			THEN UPDATE karyawan
					SET karyawan.email = email , Update_at = curdate()
					WHERE bts.karyawan.NIP like idChange;
			select concat(keteranganUpdate, "E-mail , ") into keteranganUpdate;
            -- insert history --
            select concat("Update email karyawan lama dengan NIP :",idChange," dengan email baru : ",email,".") into hasil;
			call `InsertHistory`(update_by, hasil);
			END IF;        
			
			if foto IS NOT NULL
				THEN 
					UPDATE karyawan
					SET Foto = foto , Update_at = curdate()
					WHERE karyawan.NIP like idChange;
			select concat(keteranganUpdate, "Foto , ") INTO keteranganUpdate;
            select concat("Update foto karyawan lama dengan NIP :",idChange," dengan foto baru : ",foto," .") into hasil;
            -- insert history --
            call `InsertHistory`(update_by, hasil);
			END IF; 
            
            if Tanggal_Lahir IS NOT NULL
				THEN 
					UPDATE karyawan
					SET Tanggal_Lahir = Tanggal_Lahir , Update_at = curdate()
					WHERE karyawan.NIP like idChange;
			select concat(keteranganUpdate, "Tanggal Lahir , ") INTO keteranganUpdate;
            select concat("Update tanggal lahir karyawan lama dengan NIP :",idChange," dengan tanggal lahir baru : ",Tanggal_Lahir,".") into hasil;
            -- insert history --
            call `InsertHistory`(update_by, hasil);
			END IF;  
            
            if address IS NOT NULL
				THEN UPDATE karyawan
					SET Alamat = address , Update_at = curdate()
					WHERE karyawan.NIP like idChange;
            select concat(keteranganUpdate, "Alamat , ") INTO keteranganUpdate;
            select concat("Update alamat karyawan lama dengan NIP :",idChange," dengan alamat baru : ",address," .") into hasil;
            -- insert history --
            call `InsertHistory`(update_by, hasil);
			END IF;
            
            if Tempat_Lahir IS NOT NULL
				THEN UPDATE karyawan
					SET Tempat_Lahir = Tempat_Lahir , Update_at = curdate()
					WHERE karyawan.NIP like idChange;
            select concat(keteranganUpdate, "Tempat Lahir , ") INTO keteranganUpdate;
			select concat("Update tempat lahir karyawan lama dengan NIP :",idChange," dengan tempat lahir baru : ",Tempat_Lahir," .") into hasil;
            -- insert history --
            call `InsertHistory`(update_by, hasil);

			END IF;
            
            if NPWP IS NOT NULL
				THEN UPDATE karyawan
					SET NPWP = NPWP , Update_at = curdate()
					WHERE karyawan.NIP like idChange;
            select concat(keteranganUpdate, "NPWP , ") INTO keteranganUpdate;
            select concat("Update NPWP karyawan lama dengan NIP :",idChange," dengan NPWP baru : ",NPWP," .") into hasil;
            -- insert history --
           call `InsertHistory`(update_by, hasil);
			END IF;
            
            if BPJS_TenagaKerja IS NOT NULL
				THEN UPDATE karyawan
					SET BPJS_TenagaKerja = BPJS_TenagaKerja , Update_at = curdate()
					WHERE karyawan.NIP like idChange;
            select concat(keteranganUpdate, "BPJS Tenaga Kerja , ") INTO keteranganUpdate;
            select concat("Update BPJS Tenaga Kerja karyawan lama dengan NIP :",idChange," dengan BPJS Tenaga Kerja baru : ",BPJS_TenagaKerja," .") into hasil;
            -- insert history --
            call `InsertHistory`(update_by, hasil);
			END IF;
            
            if BPJS_Kesehatan IS NOT NULL
				THEN UPDATE karyawan
					SET BPJS_Kesehatan = BPJS_Kesehatan , Update_at = curdate()
					WHERE karyawan.NIP like idChange;
            select concat(keteranganUpdate, "BPJS Kesehatan , ") INTO keteranganUpdate;
            select concat("Update BPJS Kesehatan karyawan lama dengan NIP :",idChange," dengan BPJS Kesehatan baru : ",BPJS_Kesehatan," .") into hasil;
            -- insert history --
            call `InsertHistory`(update_by, hasil);
			END IF;
            
            if Id_Div IS NOT NULL
				THEN UPDATE karyawan
					SET Id_Divisi = Id_Div , Update_at = curdate()
					WHERE karyawan.NIP like idChange;
            select concat(keteranganUpdate, "Id Divisi , ") INTO keteranganUpdate;
            select Nama_Divisi into name_divisi from divisi where divisi.Id_Divisi = Id_Div;
            select concat("Update Id Divisi karyawan lama dengan NIP :",idChange," dipindahkan ke Divisi : ",name_divisi," .") into hasil;
            -- insert history --
           call `InsertHistory`(update_by, hasil);
			END IF;
            
            if Id_Depart IS NOT NULL
				THEN UPDATE karyawan
					SET Id_Departemen = Id_Depart , Update_at = curdate()
					WHERE karyawan.NIP like idChange;
            select concat(keteranganUpdate, "Id Departemen , ") INTO keteranganUpdate;
            select Nama_Departemen into name_departemen from departemen where departemen.Id_Departemen = Id_Depart;
            select concat("Update Id Departemen karyawan lama dengan NIP :",idChange," dipindahkan ke Departemen : ",name_departemen," .") into hasil;
            -- insert history --
            call `InsertHistory`(update_by, hasil);

			END IF;
            
            if Id_Jab IS NOT NULL
				THEN UPDATE karyawan
					SET Id_Jabatan = Id_Jab , Update_at = curdate()
					WHERE karyawan.NIP like idChange;
            select concat(keteranganUpdate, "Id Jabatan , ") INTO keteranganUpdate;
            select Nama_Jabatan into name_jabatan from jabatan where jabatan.Id_Jabatan = Id_Jab;
            select concat("Update Id jabatan karyawan lama dengan NIP :",idChange," dipindahkan ke Jabatan : ",name_jabatan," .") into hasil;
            -- insert history --
            call `InsertHistory`(update_by, hasil);
			END IF;
            
            if Alamat_Kerja IS NOT NULL
				THEN UPDATE karyawan
					SET Alamat_Kerja = Alamat_Kerja , Update_at = curdate()
					WHERE karyawan.NIP like idChange;
            select concat(keteranganUpdate, "Alamat Kerja , ") INTO keteranganUpdate;
            select concat("Update Alamat kerja karyawan lama dengan NIP :",idChange," dengan Alamat Kerja baru : ",Alamat_Kerja," .") into hasil;
             -- insert history --
            call `InsertHistory`(update_by, hasil);
			END IF;
			
            if Telp_Kerja IS NOT NULL
				THEN UPDATE karyawan
					SET Telp_Kerja = Telp_Kerja , Update_at = curdate()
					WHERE karyawan.NIP like idChange;
            select concat(keteranganUpdate, "Telp Kerja , ") INTO keteranganUpdate;
            select concat("Update Telp kerja karyawan lama dengan NIP :",idChange," dengan Nomor Telpon Kerja baru : ",Telp_Kerja," .") into hasil;
            -- insert history --
            call `InsertHistory`(update_by, hasil);
			END IF;
			Select * , "UPDATED SUCCESSFULLY" as statsu, keteranganUpdate FROM karyawan WHERE karyawan.NIP like idChange;
	   else
			select concat("Karyawan dengan NIP : ",idChange," Sudah tidak begabung dalam Perusahaan.") into hasil;
			Select * , "UPDATE FAILED" as statsu, hasil FROM karyawan WHERE karyawan.NIP like idChange;
       END IF;
	END IF;   
END