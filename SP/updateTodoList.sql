CREATE DEFINER=`root`@`localhost` PROCEDURE `updateTodoList`(
Id_T int,
Nama varchar(50),
Desk varchar(250),
Status_Task varchar(20),
Id_User int(11),
Create_by int)
BEGIN
	declare count int;
    
    select count(Id_Task) into count from todo_list where Id_Task = Id_T and Delete_at is null;
    
    if count =1 
    then
		update todo_list set Nama_Task = Nama, Deskripsi = Desk, Status_Task = Status_Task, Id_User = Id_User, Update_at = curdate() where Id_Task = Id_T;
        
        insert into log(Keterangan,Create_at,Create_by) values(concat('Update todo list dengan Id_Task : ',Id_T),curdate(),Create_by);
    end if;
END