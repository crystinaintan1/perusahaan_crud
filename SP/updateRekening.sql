CREATE DEFINER=`root`@`localhost` PROCEDURE `updateRekening`(
Id_R int,
Nomor int,
Create_by int)
BEGIN
	declare count int;
    
    select count(Id_Rekening) into count from rekening where Id_Rekening = Id_R and Delete_at is null;
    
    if count = 1
    then
		update rekening set Nomor_Rekening = Nomor, Update_at = curdate() where Id_Rekening = Id_R;
        
        insert into log(Keterangan,Create_at,Create_by) values (concat('Update rekening dengan Id_Rekeking : ',Id_R),curdate(),Create_by);
    end if;
    
    select count;
END