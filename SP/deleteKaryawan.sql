CREATE DEFINER=`root`@`localhost` PROCEDURE `deleteKaryawan`(
NIP varchar(20),
Create_by int)
BEGIN
	declare id_u,id_r,id_e,id_pk,count int;
    
    select count(NIP) into count from karyawan where NIP like NIP and delete_at is null;
    
    if count = 1
    then

		update karyawan set Update_at = curdate(), delete_at = curdate() where NIP like NIP;
		update user set Update_at = curdate(), delete_at = curdate() where Id_Karyawan like NIP ;
		update rekening set Update_at = curdate(), delete_at = curdate() where NIP like NIP;
		update evaluasi set Update_at = curdate(), delete_at = curdate() where NIP like NIP;
		update proyek_karyawan set Update_at = curdate(), delete_at = curdate() where NIP like NIP;
		
		insert into log(Keterangan,Create_at,Create_by) values(concat('Delete karyawan dengan NIP : ',NIP),curdate(),Create_by);
		
		select Id_User into id_u from User where Id_Karyawan like NIP;
		insert into log (Keterangan,Create_at,Create_by) values(concat('Delete user dengan Id_User : ',id_u),curdate(),Create_by);
		
		select Id_Rekening into id_r from Rekening where NIP like NIP;
		insert into log (Keterangan,Create_at,Create_by) values(concat('Delete Rekening dengan Id_Rekening : ',id_r),curdate(),Create_by);
		
		select Id_Evaluasi into id_e from Evaluasi where NIP like NIP;
		insert into log (Keterangan,Create_at,Create_by) values(concat('Delete evaluasi dengan Id_Evaluasi : ',id_e),curdate(),Create_by);
		
		select Id_Proyek_Karyawan into id_pk from proyek_karyawan where NIP like NIP;
		insert into log (Keterangan,Create_by) values(concat('Delete Proyek Karyawan dengan Id_Proyek_Karyawan : ',id_pk),curdate(),Create_by);
	end if;
    
    select count;
END