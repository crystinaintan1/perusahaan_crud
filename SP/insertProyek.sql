CREATE DEFINER=`root`@`localhost` PROCEDURE `insertProyek`(
Nama varchar(100),
Tanggal_M date,
Tanggal_S date,
Status varchar(20),
Klien varchar(50),
Kategori varchar(50),
Create_by int)
BEGIN
	declare count,id_p int;
    
    select count(Id_Proyek) into count from proyek where Nama_Proyek like Nama and (Delete_at is not null or Delete_at is null);
    -- 0 belum ada, 1 udah ada delete_at =  null
    
    if count =0
    then
		insert into proyek (Nama_Proyek,Tanggal_Mulai,Tanggal_Selesai,Status,Klien,Kategori,Create_at) 
        values(Nama,Tanggal_M,Tanggal_S,Status,Klien,Kategori,curdate());
        
        select Id_proyek into id_p from proyek where Nama_Proyek like Nama;
        insert into log(Keterangan,Create_at,Create_by) values(concat("Menambahkan Proyek dengan Id_Proyek : ",id_p),curdate(),Create_by);
    end if;
END