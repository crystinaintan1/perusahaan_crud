CREATE DEFINER=`root`@`localhost` PROCEDURE `deleteUser`(
email_ varchar(100),
Create_by int)
BEGIN
	declare count,id_u int;
    
    select count(Id_User) into count from user where email like email_ and delete_at is null;
    
    if count = 1
    then
		update user set Update_at = curdate(), delete_at = curdate() where email like email_;
        
        select count(Id_User) into id_u from user where email like email_;
        insert into log(Keterangan,Create_at,Create_by) values (concat("Delete admin dengan Id_User : ",id_u),curdate(),Create_by);
    end if;
    
    select count;
END