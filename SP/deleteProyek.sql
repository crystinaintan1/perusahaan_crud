CREATE DEFINER=`root`@`localhost` PROCEDURE `deleteProyek`(
Id_P int,
Create_by int)
BEGIN
	declare count int;
    
    select count(Id_Proyek) into count from proyek where Id_Proyek = Id_P and delete_at is null;
    
    if count = 1
    then
		update proyek set Update_at = curdate(), Delete_at = curdate() where Id_Proyek = Id_P;
        
        insert into log(Keterangan,Create_at,Create_by) values(concat('Delete proyek dengan Id_Proyek : ',Id_P),curdate(),Create_by);
        
        update proyek_Karyawan set Delete_at = curdate() where Id_Proyek = Id_P;
        
        insert into log(Keterangan,Create_at,Create_by) values(concat('Delete proyek_karyawan dengan Id_Proyek : ',Id_P),curdate(),Create_by);
    end if;
    
    select count;
END