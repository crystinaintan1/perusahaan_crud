CREATE DEFINER=`root`@`localhost` PROCEDURE `updateStatusKaryawan`(
Id_S int,
Nama varchar(50),
Create_by int)
BEGIN
	declare count int;
    
    select count(Id_Status) into count from status_karyawan where Id_Status = Id_S and delete_at is null;
    
    if count = 1
    then
		update status_karyawan set Nama_Status = Nama, Update_at = curdate() where Id_Status = Id_S;
    
		insert into log(Keterangan,Create_at,Create_by) values(concat('Update Status Karyawan dengan Id_Status : ',Id_S),curdate(),Create_by);
    end if;
END