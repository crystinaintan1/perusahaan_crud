CREATE DEFINER=`root`@`localhost` PROCEDURE `deleteStatusKaryawan`(
Id_S int,
Create_by int)
BEGIN
	declare count int;
    
    select count(Id_Status) into count from status_karyawan where Id_Status = Id_s and Delete_at is null;
    
    if count = 1
    then
		update status_karyawan set Update_at = curdate(), Delete_at = curdate() where Id_Status = Id_S;
        
        insert into log(Keterangan,Create_at,Create_by) values(concat('Delete Status Karyawan dengan Id_Status : ',Id_S),curdate(),Create_by);
    end if;
    
    select count;
END