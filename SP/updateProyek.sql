CREATE DEFINER=`root`@`localhost` PROCEDURE `updateProyek`(
Id_Proyek_ int,
Nama varchar(100),
Tanggal_S date,
Status varchar(20),
Kategori varchar(50),
Create_by int)
BEGIN
	declare count int;
    
    select count(Id_Proyek) into count from proyek where Id_Proyek = Id_Proyek_ and delete_at is null;
    
    if count = 1
    then
		update proyek set Nama_Proyek = Nama,Tanggal_Selesai = Tanggal_S, Status = Status,Kategori = Kategori,Update_at = curdate() where Id_Proyek = Id_Proyek_;
        
        insert into log(Keterangan,Create_at,Create_by) values(concat('Update proyek dengan Id_Proyek : ',Id_Proyek_),curdate(),Create_by);
    end if;
    
    select count;
END