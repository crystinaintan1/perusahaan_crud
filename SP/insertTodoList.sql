CREATE DEFINER=`root`@`localhost` PROCEDURE `insertTodoList`(
Nama varchar(50),
Desk varbinary(250),
Status_Task varchar(20),
Create_by int)
BEGIN
	declare count,id_t int;
    
    select count(Id_Task) into count from todo_list where Nama_Task like Nama and (Delete_at is null or Delete_at is not null);
    
    if count = 0
    then
		insert into todo_list(Nama_Task,Deskripsi,Status_Task,Create_at,Id_User) values(Nama,Desk,Status_Task,curdate(),Create_by);
        
        select Id_Task into id_t from todo_list where Nama_Task like Nama;
        insert into log(Keterangan,Create_at,Create_by) values(concat('Menambahkan Todo List dengan Id_Task : ',id_t),curdate(),Create_by);
    end if;
    
	select count;
END