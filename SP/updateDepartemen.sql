CREATE DEFINER=`root`@`localhost` PROCEDURE `updateDepartemen`(
Id_D int,
Nama varchar(50),
Create_by int)
BEGIN
	declare count int;
    
    select count(Id_Departemen) into count from departemen where Id_Departemen = Id_D and delete_at is null;
    
    if count = 1
    then
		update departemen set Nama_Departemen = Nama, Update_at=curdate() where Id_Departemen = Id_D;
        
        insert into log(Keterangan,Create_at,Create_by) values(concat("Update nama Departemen dengan Id_Departemen : ",Id_D),curdate(),Create_by);
    end if;
END