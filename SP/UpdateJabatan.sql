CREATE DEFINER=`root`@`localhost` PROCEDURE `UpdateJabatan`(nama_jab_Lama varchar(50), nama_jab_Baru varchar(50), id_User int(11), id_Depart int(11), lev int(11))
BEGIN
	DECLARE cekNamaJabatan int(11);
    DECLARE deleteStatus date;
    DECLARE eksisDepartemen int(11);
    DECLARE update_by int(11);
    DECLARE hasil varchar(1000);
    DECLARE kesimpulan varchar(200);
    
    -- PADA UPDATE jABATAN INI PATOKANNYA PADA ID JABATAN. BUKAN PADA NAMA jABATAN, KARENA NAMA JABATAN KEMUNGKINAN BISA DIUPDATE JUGA--
    SELECT Id_Jabatan  INTO cekNamaJabatan from jabatan WHERE Nama_Jabatan = nama_jab_Lama;
	SELECT Id_Departemen from departemen where departemen.Id_Departemen = id_Depart into eksisDepartemen;
    SELECT id_User into update_by;
    SELECT "Berhasil merubah : " INTO kesimpulan;
    
    IF cekNamaJabatan is not null
    THEN
		SELECT Delete_at from jabatan where jabatan.Nama_Jabatan = nama_jab_Lama into deleteStatus;
        IF deleteStatus is null
        THEN
			IF nama_jab_Baru IS NOT NULL
            THEN
				UPDATE jabatan 
				SET Nama_Jabatan = nama_jab_Baru , Update_at = curdate()
                WHERE jabatan.Id_Jabatan = cekNamaJabatan;
                select CONCAT(kesimpulan ," Nama Jabatan, ") into kesimpulan;
                select concat("Update Jabatan lama dengan ID Jabatan :", cekNamaJabatan , " dengan nama baru : " , nama_jab_Baru ,".") INTO HASIL;
                -- insert history --
				call `InsertHistory`(update_by, hasil);
                -- call SP Insert Divisi_departemen -- 
            END IF;
            IF id_Depart IS NOT NULL
            THEN
				IF eksisDepartemen IS NOT NULL
                THEN
					UPDATE jabatan 
					SET Id_Departemen = id_Depart , Update_at = curdate()
					WHERE jabatan.Id_Jabatan = cekNamaJabatan;
                    select CONCAT(kesimpulan ," ID Departemen pada Jabatan, ") into kesimpulan;
					select concat("Update Jabatan lama dengan ID Jabatan :", cekNamaJabatan , " dengan Id Departemen baru : " , id_Depart ,".") INTO HASIL;
					-- insert history --
					call `InsertHistory`(update_by, hasil);
					-- call SP Insert Divisi_departemen -- 
                END IF;
            END IF;
            IF lev IS NOT NULL
            THEN
				UPDATE jabatan 
				SET Level = lev , Update_at = curdate()
                WHERE jabatan.Id_Jabatan = cekNamaJabatan;
                select CONCAT(kesimpulan ," Level Jabatan, ") into kesimpulan;
                select concat("Update Jabatan lama dengan ID Jabatan :", cekNamaJabatan , " dengan Level baru : " , lev ,".") INTO HASIL;
                -- insert history --
				call `InsertHistory`(update_by, hasil);
                -- call SP Insert Divisi_departemen -- 
            END IF;
            select substring(kesimpulan, 1, length(kesimpulan)-2) into kesimpulan;
            select kesimpulan;
		ELSE
                select "Jabatan tidak ditemukan atau sudah dihapus." into kesimpulan;
                select kesimpulan;
		END IF;
    ELSE
		SELECT concat("Tidak ada Jabatan dengan nama : ", nama_jab_Lama) into kesimpulan;
        select kesimpulan;
    END IF;
END