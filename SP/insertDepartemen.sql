CREATE DEFINER=`root`@`localhost` PROCEDURE `insertDepartemen`(
Nama_Departemen_ varchar(50),
Create_by int,
Id_Divisi int)
BEGIN
	declare count,id_d int;
    
    select count(Id_Departemen) into count from departemen where Nama_Departemen like Nama_Departemen_ and Delete_at is not null;
    -- 0 belum ada, 1 udah ada delete_at =  null
    
    if count = 0
    then
		insert into departemen (Nama_Departemen,Create_at,Id_Divisi) values (Nama_Departemen_,curdate(),Id_Divisi);
        
        select Id_departemen into id_d from Departemen where Nama_Departemen like Nama_Departemen_;
        insert into log(Keterangan,Create_at,Create_by) values (concat('Menambahkan Departemen dengan Id_Departemen : ',id_d),curdate(),Create_by);
    else
		update departemen set Create_at = curdate(), Update_at=curdate(), Delete_at = null where Nama_Departemen like Nama_Departemen_;
        
        select Id_departemen into id_d from Departemen where Nama_Departemen like Nama_Departemen_;
        insert into log(Keterangan,Create_at,Create_by) values (concat('Update Departemen (delete_at) dengan Id_Departemen : ',id_d),curdate(),Create_by);
    end if;
END