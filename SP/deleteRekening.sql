CREATE DEFINER=`root`@`localhost` PROCEDURE `deleteRekening`(
Id_R int,
Create_by int)
BEGIN
	declare count int;
    
    select count(Id_Rekening) into count from rekening where Id_Rekening = Id_R and Delete_at is null;
    
    if count = 1
    then
		update rekening set Update_at = curdate(), Delete_at = curdate() where Id_Rekening = Id_R;
        
        insert into log(Keterangan,Create_at,Create_by) values (concat('Delete rekening dengan Id_Rekening : ',Id_R),curdate(),Create_by);
    end if;
    
    select count;
END