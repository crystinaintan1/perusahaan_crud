CREATE DEFINER=`root`@`localhost` PROCEDURE `deleteDepartemen`(
Id_D int,
Create_by int)
BEGIN
	declare count int;
    select count(Id_Departemen) into count from departemen where Id_Departemen = Id_D and delete_at is null;
    if count = 1
    then
        update departemen set Update_at = curdate(), Delete_at = curdate() where Nama_Departemen like Nama;
        
        insert into log(Keterangan,Create_at,Create_by) values(concat("Delete Departemen dengan Id_Departemen : ",Id_D),curdate(),Create_by);
    end if;
    select count;
END