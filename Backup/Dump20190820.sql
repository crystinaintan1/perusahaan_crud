-- MySQL dump 10.13  Distrib 8.0.16, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: bts
-- ------------------------------------------------------
-- Server version	8.0.16

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `departemen`
--

DROP TABLE IF EXISTS `departemen`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `departemen` (
  `Id_Departemen` int(11) NOT NULL AUTO_INCREMENT,
  `Nama_Departemen` varchar(50) NOT NULL,
  `Create_at` date NOT NULL,
  `Update_at` date DEFAULT NULL,
  `Delete_at` date DEFAULT NULL,
  `Id_Divisi` int(11) NOT NULL,
  PRIMARY KEY (`Id_Departemen`),
  KEY `Id_Divisi5_idx` (`Id_Divisi`),
  CONSTRAINT `Id_Divisi5` FOREIGN KEY (`Id_Divisi`) REFERENCES `divisi` (`Id_Divisi`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `departemen`
--

LOCK TABLES `departemen` WRITE;
/*!40000 ALTER TABLE `departemen` DISABLE KEYS */;
INSERT INTO `departemen` VALUES (3,'A','2019-08-01',NULL,NULL,3);
/*!40000 ALTER TABLE `departemen` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `divisi`
--

DROP TABLE IF EXISTS `divisi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `divisi` (
  `Id_Divisi` int(11) NOT NULL AUTO_INCREMENT,
  `Nama_Divisi` varchar(50) NOT NULL,
  `Create_at` date NOT NULL,
  `Update_at` date DEFAULT NULL,
  `Delete_at` date DEFAULT NULL,
  PRIMARY KEY (`Id_Divisi`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `divisi`
--

LOCK TABLES `divisi` WRITE;
/*!40000 ALTER TABLE `divisi` DISABLE KEYS */;
INSERT INTO `divisi` VALUES (1,'A','2029-07-29','2019-08-01','2019-08-01'),(3,'A','2019-08-01','2019-08-01','2019-08-01'),(4,'Art','2019-08-02','2019-08-02',NULL),(5,'Lili','2019-08-01','2019-08-02',NULL),(6,'Lilis','2019-08-01','2019-08-01','2019-08-01'),(7,'Kiki','2019-08-01','2019-08-01','2019-08-01'),(8,'Lilisan','2019-08-01','2019-08-01',NULL);
/*!40000 ALTER TABLE `divisi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `divisi_departemen`
--

DROP TABLE IF EXISTS `divisi_departemen`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `divisi_departemen` (
  `Id_Divisi_Departemen` int(11) NOT NULL AUTO_INCREMENT,
  `Id_Divisi` int(11) NOT NULL,
  `Id_Departemen` int(11) NOT NULL,
  `Create_at` date NOT NULL,
  `Update_at` date DEFAULT NULL,
  `Delete_at` date DEFAULT NULL,
  PRIMARY KEY (`Id_Divisi_Departemen`),
  KEY `Id_Divisi_idx` (`Id_Divisi`),
  KEY `Id_Departemen_idx` (`Id_Departemen`),
  CONSTRAINT `Id_Departemen` FOREIGN KEY (`Id_Departemen`) REFERENCES `departemen` (`Id_Departemen`),
  CONSTRAINT `Id_Divisi` FOREIGN KEY (`Id_Divisi`) REFERENCES `divisi` (`Id_Divisi`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `divisi_departemen`
--

LOCK TABLES `divisi_departemen` WRITE;
/*!40000 ALTER TABLE `divisi_departemen` DISABLE KEYS */;
/*!40000 ALTER TABLE `divisi_departemen` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `evaluasi`
--

DROP TABLE IF EXISTS `evaluasi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `evaluasi` (
  `Id_Evaluasi` int(11) NOT NULL AUTO_INCREMENT,
  `Tanggal_Masuk` date NOT NULL,
  `Masa_kerja` date DEFAULT NULL,
  `Tanggal_Evaluasi` date DEFAULT NULL,
  `Id_Status` int(11) NOT NULL,
  `Tanggal_Resign` date DEFAULT NULL,
  `Create_at` date NOT NULL,
  `Update_at` date DEFAULT NULL,
  `Delete_at` date DEFAULT NULL,
  `NIP` varchar(20) NOT NULL,
  PRIMARY KEY (`Id_Evaluasi`),
  KEY `NIP1_idx` (`NIP`),
  CONSTRAINT `NIP1` FOREIGN KEY (`NIP`) REFERENCES `karyawan` (`NIP`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `evaluasi`
--

LOCK TABLES `evaluasi` WRITE;
/*!40000 ALTER TABLE `evaluasi` DISABLE KEYS */;
/*!40000 ALTER TABLE `evaluasi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jabatan`
--

DROP TABLE IF EXISTS `jabatan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `jabatan` (
  `Id_Jabatan` int(11) NOT NULL AUTO_INCREMENT,
  `Nama_Jabatan` varchar(50) NOT NULL,
  `Create_at` date NOT NULL,
  `Update_at` date DEFAULT NULL,
  `Delete_at` date DEFAULT NULL,
  `Id_Departemen` int(11) NOT NULL,
  `Level` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id_Jabatan`),
  KEY `Id_Departemen4_idx` (`Id_Departemen`),
  CONSTRAINT `Id_Departemen4` FOREIGN KEY (`Id_Departemen`) REFERENCES `departemen` (`Id_Departemen`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jabatan`
--

LOCK TABLES `jabatan` WRITE;
/*!40000 ALTER TABLE `jabatan` DISABLE KEYS */;
INSERT INTO `jabatan` VALUES (3,'A','2019-08-01','2019-08-01',NULL,3,3),(4,'jabatan1','2019-08-01',NULL,NULL,3,1),(5,'Pel','2019-08-01','2019-08-02',NULL,3,10),(6,'jabatan5','2019-08-02','2019-08-02',NULL,3,10);
/*!40000 ALTER TABLE `jabatan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `karyawan`
--

DROP TABLE IF EXISTS `karyawan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `karyawan` (
  `NIP` varchar(20) NOT NULL,
  `Nama` varchar(300) NOT NULL,
  `email` varchar(100) NOT NULL,
  `Tempat_Lahir` varchar(50) NOT NULL,
  `Tanggal_Lahir` date NOT NULL,
  `Alamat` varchar(250) NOT NULL,
  `NPWP` varchar(200) DEFAULT NULL,
  `BPJS_TenagaKerja` varchar(250) NOT NULL,
  `BPJS_Kesehatan` varchar(250) NOT NULL,
  `Create_at` date NOT NULL,
  `Update_at` date DEFAULT NULL,
  `Delete_at` date DEFAULT NULL,
  `Foto` longtext NOT NULL,
  `Id_Divisi` int(11) NOT NULL,
  `Id_Departemen` int(11) NOT NULL,
  `Id_Jabatan` int(11) NOT NULL,
  `Alamat_Kerja` varchar(250) NOT NULL,
  `Telp_Kerja` varchar(200) NOT NULL,
  PRIMARY KEY (`NIP`),
  KEY `Id_Divisi1_idx` (`Id_Divisi`),
  KEY `Id_Departemen1_idx` (`Id_Departemen`),
  KEY `Id_Jabatan1_idx` (`Id_Jabatan`),
  CONSTRAINT `Id_Departemen1` FOREIGN KEY (`Id_Departemen`) REFERENCES `departemen` (`Id_Departemen`),
  CONSTRAINT `Id_Divisi1` FOREIGN KEY (`Id_Divisi`) REFERENCES `divisi` (`Id_Divisi`),
  CONSTRAINT `Id_Jabatan1` FOREIGN KEY (`Id_Jabatan`) REFERENCES `jabatan` (`Id_Jabatan`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `karyawan`
--

LOCK TABLES `karyawan` WRITE;
/*!40000 ALTER TABLE `karyawan` DISABLE KEYS */;
INSERT INTO `karyawan` VALUES ('111','Husnul','Hakim@gmail.com','','1989-12-10','','','','','2019-07-19','2019-08-05',NULL,'sadsfdfdfsd',3,3,5,'',''),('1313131313','Intan','Intan@gmail.com','Palembang','1990-03-13','Jalan Raya 1','2eaefew3diiiii','123457908jshdj','00000000jshdj0','2019-07-30',NULL,NULL,'aaaaaaaaaaaa',3,3,3,'Jalan Merdeka','000000000002');
/*!40000 ALTER TABLE `karyawan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `log`
--

DROP TABLE IF EXISTS `log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `log` (
  `Id_Log` int(11) NOT NULL AUTO_INCREMENT,
  `Keterangan` varchar(1000) NOT NULL,
  `Create_at` date NOT NULL,
  `Create_by` int(11) NOT NULL,
  PRIMARY KEY (`Id_Log`)
) ENGINE=InnoDB AUTO_INCREMENT=212 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `log`
--

LOCK TABLES `log` WRITE;
/*!40000 ALTER TABLE `log` DISABLE KEYS */;
INSERT INTO `log` VALUES (43,'Update data karyawan dengan NIP : ','2019-07-31',1),(44,'Update data karyawan dengan NIP : ','2019-07-31',1),(45,'Update data user dengan NIP : ','2019-07-31',1),(46,'Memasukkan data karyawan dengan NIP : ','2019-07-31',1),(47,'Memasukkan data User dengan Id_User : ','2019-07-31',1),(48,'Delete karyawan dengan NIP : 123','2019-07-31',1),(49,'Delete karyawan dengan NIP : 111','2019-07-31',1),(50,'Delete karyawan dengan NIP : 111','2019-07-31',1),(51,'Delete karyawan dengan NIP : 111','2019-07-31',1),(52,'Delete user dengan Id_User : 18','2019-07-31',1),(53,'Delete Rekening dengan Id_Rekening : 1','2019-07-31',1),(54,'Evaluasi dengan Id_Evaluasi : 1','2019-07-31',1),(55,'Update data karyawan dengan NIP : 111','2019-07-31',1),(56,'Update data user dengan NIP : 18','2019-07-31',1),(57,'Menambahkan admin dengan Id_User : 19','2019-07-31',1),(58,'Delete admin dengan Id_User : 1','2019-07-31',1),(59,'Update data user Password dengan Id_User19','2019-07-31',1),(60,'Update Departemen (delete_at) dengan Id_Departemen : 1','2019-07-31',1),(61,'Update Departemen (delete_at) dengan Id_Departemen : 1','2019-07-31',1),(62,'Menambahkan Departemen dengan Id_Departemen : 2','2019-07-31',1),(63,'Delete Departemen dengan Id_Departemen : 2','2019-07-31',1),(64,'Delete Departemen dengan Id_Departemen : 2','2019-07-31',1),(65,'Update nama Departemen dengan Id_Departemen : 1','2019-07-31',1),(66,'Menambahkan Proyek dengan Id_Proyek : 2','2019-07-31',1),(67,'Memasukkan data karyawan dengan NIP : 111','2019-08-01',1),(68,'Memasukkan data User dengan Id_User : 20','2019-08-01',1),(69,'Update nama karyawan lama dengan NIP :111 dengan nama baru : Intan.','2019-08-01',1),(70,'Update email karyawan lama dengan NIP :111 dengan email baru : Intan@gmail.com.','2019-08-01',1),(71,'Update foto karyawan lama dengan NIP :111 dengan foto baru : sadsfdfdfsd .','2019-08-01',1),(72,'Update tanggal lahir karyawan lama dengan NIP :111 dengan tanggal lahir baru : 1998-03-13.','2019-08-01',1),(73,'Update alamat karyawan lama dengan NIP :111 dengan alamat baru : zzzz .','2019-08-01',1),(74,'Update tempat lahir karyawan lama dengan NIP :111 dengan tempat lahir baru : zzzz .','2019-08-01',1),(75,'Update NPWP karyawan lama dengan NIP :111 dengan NPWP baru : aaaaaaaaa .','2019-08-01',1),(76,'Update BPJS Tenaga Kerja karyawan lama dengan NIP :111 dengan BPJS Tenaga Kerja baru : aaaaaaaaa .','2019-08-01',1),(77,'Update BPJS Kesehatan karyawan lama dengan NIP :111 dengan BPJS Kesehatan baru : aaaaaaaa .','2019-08-01',1),(78,'Update Alamat kerja karyawan lama dengan NIP :111 dengan Alamat Kerja baru : aaaaaaaa .','2019-08-01',1),(79,'Update Telp kerja karyawan lama dengan NIP :111 dengan Nomor Telpon Kerja baru : aaaaaaaa .','2019-08-01',1),(80,'Update nama karyawan lama dengan NIP :111 dengan nama baru : Husnul.','2019-08-01',1),(81,'Update email karyawan lama dengan NIP :111 dengan email baru : Hakim@gmail.com.','2019-08-01',1),(82,'Update foto karyawan lama dengan NIP :111 dengan foto baru : sadsfdfdfsd .','2019-08-01',1),(83,'Update tanggal lahir karyawan lama dengan NIP :111 dengan tanggal lahir baru : 1989-12-10.','2019-08-01',1),(84,'Update alamat karyawan lama dengan NIP :111 dengan alamat baru :  .','2019-08-01',1),(85,'Update tempat lahir karyawan lama dengan NIP :111 dengan tempat lahir baru :  .','2019-08-01',1),(86,'Update NPWP karyawan lama dengan NIP :111 dengan NPWP baru :  .','2019-08-01',1),(87,'Update BPJS Tenaga Kerja karyawan lama dengan NIP :111 dengan BPJS Tenaga Kerja baru :  .','2019-08-01',1),(88,'Update BPJS Kesehatan karyawan lama dengan NIP :111 dengan BPJS Kesehatan baru :  .','2019-08-01',1),(89,'Update Alamat kerja karyawan lama dengan NIP :111 dengan Alamat Kerja baru :  .','2019-08-01',1),(90,'Update Telp kerja karyawan lama dengan NIP :111 dengan Nomor Telpon Kerja baru :  .','2019-08-01',1),(91,'Update Divisi baru dengan nama : Kiki.','2019-08-01',1),(92,'Update Divisi lama dengan ID Divisi : \'Lilia\' , dengan nama baru : ? .','2019-08-01',6),(93,'Update Divisi lama dengan ID Divisi : 6 , dengan nama baru : \'Lilia\' .','2019-08-01',6),(94,'Update Divisi lama dengan ID Divisi : 6 , dengan nama baru : \'Lilis\' .','2019-08-01',6),(95,'Tambah Divisi baru dengan nama : Kiki.','2019-08-01',1),(96,'Update Divisi lama dengan ID Divisi : 6 , dengan nama baru : \'Lilis\' .','2019-08-01',6),(97,'Update Divisi lama dengan ID Divisi : 6 , dengan nama baru : \'Lilis\' .','2019-08-01',6),(98,'Update Divisi lama dengan ID Divisi : 6 , dengan nama baru : \'Lilis\' .','2019-08-01',1),(99,'Delete Divisi \'Lilis\' .','2019-08-01',1),(100,'Delete Divisi \'Lilis\' .','2019-08-01',1),(101,'Delete Divisi \'Lili\' .','2019-08-01',1),(102,'Update Divisi lama dengan ID Divisi :5 dengan nama baru : Lili.','2019-08-01',1),(103,'Delete Divisi \'Lili\' .','2019-08-01',1),(104,'Update Divisi lama dengan ID Divisi :5 dengan nama baru : Lili.','2019-08-01',1),(105,'Delete Divisi \'Lili\' .','2019-08-01',1),(106,'Tambah Divisi lama dengan ID Divisi :5 dengan nama baru : Lili.','2019-08-01',1),(107,'Tambah Divisi baru dengan nama : Lilisan.','2019-08-01',1),(108,'Tambah Divisi baru dengan nama : jabatan1.','2019-08-01',1),(109,'Tambah Jabatan baru dengan nama : jabatan2.','2019-08-01',1),(110,'Update Jabatan lama dengan ID Jabatan :5 dengan nama baru : Indah.','2019-08-01',1),(111,'Update Jabatan lama dengan ID Jabatan :5 dengan Id Departemen baru : 3.','2019-08-01',1),(112,'Update Jabatan lama dengan ID Jabatan :5 dengan Level baru : 10.','2019-08-01',1),(113,'Update Jabatan lama dengan ID Jabatan :5 dengan nama baru : Dunia.','2019-08-01',1),(114,'Update Jabatan lama dengan ID Jabatan :5 dengan Id Departemen baru : 3.','2019-08-01',1),(115,'Update Jabatan lama dengan ID Jabatan :5 dengan Level baru : 10.','2019-08-01',1),(116,'Update Jabatan lama dengan ID Jabatan :5 dengan nama baru : Pelangi.','2019-08-01',1),(117,'Update Jabatan lama dengan ID Jabatan :5 dengan Id Departemen baru : 3.','2019-08-01',1),(118,'Update Jabatan lama dengan ID Jabatan :5 dengan Level baru : 10.','2019-08-01',1),(119,'Update Jabatan lama dengan ID Jabatan :5 dengan nama baru : Pelangi.','2019-08-01',1),(120,'Update Jabatan lama dengan ID Jabatan :5 dengan Id Departemen baru : 3.','2019-08-01',1),(121,'Update Jabatan lama dengan ID Jabatan :5 dengan Level baru : 10.','2019-08-01',1),(122,'Update Jabatan lama dengan ID Jabatan :5 dengan Id Departemen baru : 3.','2019-08-01',1),(123,'Update Jabatan lama dengan ID Jabatan :5 dengan Level baru : 15.','2019-08-01',1),(124,'Update Jabatan lama dengan ID Jabatan :5 dengan nama baru : Pel-Pel.','2019-08-01',1),(125,'Update Jabatan lama dengan ID Jabatan :5 dengan Id Departemen baru : 3.','2019-08-01',1),(126,'Update Jabatan lama dengan ID Jabatan :5 dengan Level baru : 15.','2019-08-01',1),(127,'Delete Divisi \'A\' .','2019-08-01',1),(128,'Delete Divisi \'Lilisan\' .','2019-08-01',1),(129,'Tambah Divisi lama dengan ID Divisi :8 dengan nama baru : Lilisan.','2019-08-01',1),(130,'Delete Divisi \'Art\' .','2019-08-01',1),(131,'Tambah Divisi lama dengan ID Divisi :4 dengan nama baru : Art.','2019-08-01',1),(132,'Update Jabatan lama dengan ID Jabatan :5 dengan nama baru : Pelangi.','2019-08-01',1),(133,'Update Jabatan lama dengan ID Jabatan :5 dengan Id Departemen baru : 3.','2019-08-01',1),(134,'Update Jabatan lama dengan ID Jabatan :5 dengan Level baru : 15.','2019-08-01',1),(135,'Update Jabatan lama dengan ID Jabatan :5 dengan nama baru : Pel.','2019-08-01',1),(136,'Update Jabatan lama dengan ID Jabatan :5 dengan Id Departemen baru : 3.','2019-08-01',1),(137,'Update Jabatan lama dengan ID Jabatan :5 dengan Level baru : 15.','2019-08-01',1),(138,'Update Jabatan lama dengan ID Jabatan :5 dengan nama baru : Pel.','2019-08-01',1),(139,'Update Jabatan lama dengan ID Jabatan :5 dengan Id Departemen baru : 3.','2019-08-01',1),(140,'Update Jabatan lama dengan ID Jabatan :5 dengan Level baru : 15.','2019-08-01',1),(141,'Update Jabatan lama dengan ID Jabatan :5 dengan nama baru : Pel.','2019-08-01',1),(142,'Update Jabatan lama dengan ID Jabatan :5 dengan Id Departemen baru : 3.','2019-08-01',1),(143,'Update Jabatan lama dengan ID Jabatan :5 dengan Level baru : 15.','2019-08-01',1),(144,'Update Jabatan lama dengan ID Jabatan :5 dengan nama baru : Pelangi.','2019-08-01',1),(145,'Update Jabatan lama dengan ID Jabatan :5 dengan Id Departemen baru : 3.','2019-08-01',1),(146,'Update Jabatan lama dengan ID Jabatan :5 dengan Level baru : 15.','2019-08-01',1),(147,'Update Jabatan lama dengan ID Jabatan :5 dengan nama baru : Pel.','2019-08-01',1),(148,'Update Jabatan lama dengan ID Jabatan :5 dengan Id Departemen baru : 3.','2019-08-01',1),(149,'Update Jabatan lama dengan ID Jabatan :5 dengan Level baru : 15.','2019-08-01',1),(150,'Update Jabatan lama dengan ID Jabatan :5 dengan nama baru : Pel.','2019-08-01',1),(151,'Update Jabatan lama dengan ID Jabatan :5 dengan Id Departemen baru : 3.','2019-08-01',1),(152,'Update Jabatan lama dengan ID Jabatan :5 dengan Level baru : 15.','2019-08-01',1),(153,'Update Jabatan lama dengan ID Jabatan :5 dengan nama baru : Pel.','2019-08-01',1),(154,'Update Jabatan lama dengan ID Jabatan :5 dengan Id Departemen baru : 3.','2019-08-01',1),(155,'Update Jabatan lama dengan ID Jabatan :5 dengan Level baru : 10.','2019-08-01',1),(156,'Delete Jabatan \'A\' .','2019-08-01',1),(157,'Tambah jabatan lama dengan ID Jabatan :3 dengan nama baru : A.','2019-08-01',1),(158,'Delete Jabatan \'A\' .','2019-08-01',1),(159,'Tambah jabatan lama dengan ID Jabatan :3 dengan nama baru : A.','2019-08-01',1),(160,'Update Divisi lama dengan ID Divisi : 4 , dengan nama baru : \'Lilis\' .','2019-08-02',1),(161,'Update Divisi lama dengan ID Divisi : 4 , dengan nama baru : \'Art\' .','2019-08-02',1),(162,'Delete Divisi \'Art\' .','2019-08-02',1),(163,'Tambah Divisi lama dengan ID Divisi :4 dengan nama baru : Art.','2019-08-02',1),(164,'Update Divisi lama dengan ID Divisi : 4 , dengan nama baru : \'Pelangi\' .','2019-08-02',1),(165,'Update Divisi lama dengan ID Divisi : 4 , dengan nama baru : \'Art\' .','2019-08-02',1),(166,'Delete Divisi \'Art\' .','2019-08-02',1),(167,'Tambah Divisi lama dengan ID Divisi :4 dengan nama baru : Art.','2019-08-02',1),(168,'Update Divisi lama dengan ID Divisi : 4 , dengan nama baru : \'Art\' .','2019-08-02',1),(169,'Update Divisi lama dengan ID Divisi : 4 , dengan nama baru : \'Art\' .','2019-08-02',1),(170,'Update Divisi lama dengan ID Divisi : 5 , dengan nama baru : \'Lili\' .','2019-08-02',1),(171,'Update Divisi lama dengan ID Divisi : 4 , dengan nama baru : \'Art\' .','2019-08-02',1),(172,'Update Divisi lama dengan ID Divisi : 4 , dengan nama baru : \'Art\' .','2019-08-02',1),(173,'Update Divisi lama dengan ID Divisi : 5 , dengan nama baru : \'Lili\' .','2019-08-02',1),(174,'Update Divisi lama dengan ID Divisi : 4 , dengan nama baru : \'Art\' .','2019-08-02',1),(175,'Tambah Jabatan baru dengan nama : jabatan5.','2019-08-02',1),(176,'Update Jabatan lama dengan ID Jabatan :5 dengan nama baru : Pel.','2019-08-02',1),(177,'Update Jabatan lama dengan ID Jabatan :5 dengan Id Departemen baru : 3.','2019-08-02',1),(178,'Update Jabatan lama dengan ID Jabatan :5 dengan Level baru : 100.','2019-08-02',1),(179,'Update Jabatan lama dengan ID Jabatan :5 dengan nama baru : Pel.','2019-08-02',1),(180,'Update Jabatan lama dengan ID Jabatan :5 dengan Id Departemen baru : 3.','2019-08-02',1),(181,'Update Jabatan lama dengan ID Jabatan :5 dengan Level baru : 100.','2019-08-02',1),(182,'Update Jabatan lama dengan ID Jabatan :5 dengan nama baru : Pel.','2019-08-02',1),(183,'Update Jabatan lama dengan ID Jabatan :5 dengan Id Departemen baru : 3.','2019-08-02',1),(184,'Update Jabatan lama dengan ID Jabatan :5 dengan Level baru : 10.','2019-08-02',1),(185,'Delete Jabatan \'jabatan5\' .','2019-08-02',1),(186,'Tambah jabatan lama dengan ID Jabatan :6 dengan nama baru : jabatan5.','2019-08-02',1),(187,'Delete Jabatan \'jabatan5\' .','2019-08-02',1),(188,'Tambah jabatan lama dengan ID Jabatan :6 dengan nama baru : jabatan5.','2019-08-02',1),(189,'Update nama karyawan lama dengan NIP :111 dengan nama baru : Husnul.','2019-08-02',1),(190,'Update email karyawan lama dengan NIP :111 dengan email baru : Hakim@gmail.com.','2019-08-02',1),(191,'Update foto karyawan lama dengan NIP :111 dengan foto baru : sadsfdfdfsd .','2019-08-02',1),(192,'Update tanggal lahir karyawan lama dengan NIP :111 dengan tanggal lahir baru : 1989-12-10.','2019-08-02',1),(193,'Update alamat karyawan lama dengan NIP :111 dengan alamat baru :  .','2019-08-02',1),(194,'Update tempat lahir karyawan lama dengan NIP :111 dengan tempat lahir baru :  .','2019-08-02',1),(195,'Update NPWP karyawan lama dengan NIP :111 dengan NPWP baru :  .','2019-08-02',1),(196,'Update BPJS Tenaga Kerja karyawan lama dengan NIP :111 dengan BPJS Tenaga Kerja baru :  .','2019-08-02',1),(197,'Update BPJS Kesehatan karyawan lama dengan NIP :111 dengan BPJS Kesehatan baru :  .','2019-08-02',1),(198,'Update Alamat kerja karyawan lama dengan NIP :111 dengan Alamat Kerja baru :  .','2019-08-02',1),(199,'Update Telp kerja karyawan lama dengan NIP :111 dengan Nomor Telpon Kerja baru :  .','2019-08-02',1),(200,'Update nama karyawan lama dengan NIP :111 dengan nama baru : Husnul.','2019-08-05',1),(201,'Update email karyawan lama dengan NIP :111 dengan email baru : Hakim@gmail.com.','2019-08-05',1),(202,'Update foto karyawan lama dengan NIP :111 dengan foto baru : sadsfdfdfsd .','2019-08-05',1),(203,'Update tanggal lahir karyawan lama dengan NIP :111 dengan tanggal lahir baru : 1989-12-10.','2019-08-05',1),(204,'Update alamat karyawan lama dengan NIP :111 dengan alamat baru :  .','2019-08-05',1),(205,'Update tempat lahir karyawan lama dengan NIP :111 dengan tempat lahir baru :  .','2019-08-05',1),(206,'Update NPWP karyawan lama dengan NIP :111 dengan NPWP baru :  .','2019-08-05',1),(207,'Update BPJS Tenaga Kerja karyawan lama dengan NIP :111 dengan BPJS Tenaga Kerja baru :  .','2019-08-05',1),(208,'Update BPJS Kesehatan karyawan lama dengan NIP :111 dengan BPJS Kesehatan baru :  .','2019-08-05',1),(209,'Update Id jabatan karyawan lama dengan NIP :111 dipindahkan ke Jabatan : Pel .','2019-08-05',1),(210,'Update Alamat kerja karyawan lama dengan NIP :111 dengan Alamat Kerja baru :  .','2019-08-05',1),(211,'Update Telp kerja karyawan lama dengan NIP :111 dengan Nomor Telpon Kerja baru :  .','2019-08-05',1);
/*!40000 ALTER TABLE `log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proyek`
--

DROP TABLE IF EXISTS `proyek`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `proyek` (
  `Id_Proyek` int(11) NOT NULL AUTO_INCREMENT,
  `Nama_Proyek` varchar(100) NOT NULL,
  `Tanggal_Mulai` date NOT NULL,
  `Tanggal_Selesai` date NOT NULL,
  `Status` varchar(20) NOT NULL,
  `Klien` varchar(50) NOT NULL,
  `kategori` varchar(50) NOT NULL,
  `Create_at` date NOT NULL,
  `Update_at` date DEFAULT NULL,
  `Delete_at` date DEFAULT NULL,
  PRIMARY KEY (`Id_Proyek`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proyek`
--

LOCK TABLES `proyek` WRITE;
/*!40000 ALTER TABLE `proyek` DISABLE KEYS */;
INSERT INTO `proyek` VALUES (1,'BTS','2019-07-30','2019-09-12','On Progress','BTS','Gatau','2019-07-30',NULL,NULL),(2,'ABC','2019-07-16','2019-12-12','Selesai','BTS','Kerja','2019-07-31',NULL,NULL);
/*!40000 ALTER TABLE `proyek` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proyek_karyawan`
--

DROP TABLE IF EXISTS `proyek_karyawan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `proyek_karyawan` (
  `Id_Proyek_Karyawan` int(11) NOT NULL AUTO_INCREMENT,
  `Id_Karyawan` varchar(20) NOT NULL,
  `Id_Proyek` int(11) NOT NULL,
  `Create_at` date NOT NULL,
  `Update_at` date DEFAULT NULL,
  `Delete_at` date DEFAULT NULL,
  PRIMARY KEY (`Id_Proyek_Karyawan`),
  KEY `Id_Karyawan1_idx` (`Id_Karyawan`),
  KEY `Id_Proyek1_idx` (`Id_Proyek`),
  CONSTRAINT `Id_Karyawan1` FOREIGN KEY (`Id_Karyawan`) REFERENCES `karyawan` (`NIP`),
  CONSTRAINT `Id_Proyek1` FOREIGN KEY (`Id_Proyek`) REFERENCES `proyek` (`Id_Proyek`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proyek_karyawan`
--

LOCK TABLES `proyek_karyawan` WRITE;
/*!40000 ALTER TABLE `proyek_karyawan` DISABLE KEYS */;
/*!40000 ALTER TABLE `proyek_karyawan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rekening`
--

DROP TABLE IF EXISTS `rekening`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `rekening` (
  `Id_Rekening` int(11) NOT NULL AUTO_INCREMENT,
  `Nomor_Rekening` int(11) DEFAULT NULL,
  `Create_at` date NOT NULL,
  `Update_at` date DEFAULT NULL,
  `Delete_at` date DEFAULT NULL,
  `NIP` varchar(20) NOT NULL,
  PRIMARY KEY (`Id_Rekening`),
  KEY `NIP_idx` (`NIP`),
  CONSTRAINT `NIP` FOREIGN KEY (`NIP`) REFERENCES `karyawan` (`NIP`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rekening`
--

LOCK TABLES `rekening` WRITE;
/*!40000 ALTER TABLE `rekening` DISABLE KEYS */;
/*!40000 ALTER TABLE `rekening` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `status_karyawan`
--

DROP TABLE IF EXISTS `status_karyawan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `status_karyawan` (
  `Id_Status` int(11) NOT NULL AUTO_INCREMENT,
  `Nama_Status` varchar(50) NOT NULL,
  `Create_at` date NOT NULL,
  `Update_at` date DEFAULT NULL,
  `Delete_at` date DEFAULT NULL,
  PRIMARY KEY (`Id_Status`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `status_karyawan`
--

LOCK TABLES `status_karyawan` WRITE;
/*!40000 ALTER TABLE `status_karyawan` DISABLE KEYS */;
INSERT INTO `status_karyawan` VALUES (1,'Tetap','2019-07-30',NULL,NULL),(2,'Tetap','2019-07-30',NULL,NULL);
/*!40000 ALTER TABLE `status_karyawan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `todo_list`
--

DROP TABLE IF EXISTS `todo_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `todo_list` (
  `Id_Task` int(11) NOT NULL AUTO_INCREMENT,
  `Nama_Task` varchar(50) NOT NULL,
  `Deskripsi` varchar(250) NOT NULL,
  `Status_Task` varchar(20) NOT NULL,
  `Create_at` date NOT NULL,
  `Update_at` date DEFAULT NULL,
  `Delete_at` date DEFAULT NULL,
  `Id_User` int(11) NOT NULL,
  PRIMARY KEY (`Id_Task`),
  KEY `Id_User_idx` (`Id_User`),
  CONSTRAINT `Id_User` FOREIGN KEY (`Id_User`) REFERENCES `user` (`Id_User`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `todo_list`
--

LOCK TABLES `todo_list` WRITE;
/*!40000 ALTER TABLE `todo_list` DISABLE KEYS */;
/*!40000 ALTER TABLE `todo_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `user` (
  `Id_User` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(100) DEFAULT NULL,
  `Password` varchar(50) NOT NULL,
  `Role` tinyint(2) NOT NULL,
  `Create_at` date NOT NULL,
  `Update_at` date DEFAULT NULL,
  `Delete_at` date DEFAULT NULL,
  `Id_Karyawan` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`Id_User`),
  KEY `Id_karyawan_idx` (`Id_Karyawan`),
  CONSTRAINT `Id_karyawan` FOREIGN KEY (`Id_Karyawan`) REFERENCES `karyawan` (`NIP`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'hcm@bts.id','password',0,'2019-07-29',NULL,NULL,NULL),(19,'test@bts.id','Pass123',0,'2019-07-31',NULL,NULL,NULL),(20,'hash54sdds@gmail.com','password',1,'2019-08-01',NULL,NULL,'111');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'bts'
--
/*!50003 DROP PROCEDURE IF EXISTS `InsertDivisi` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `InsertDivisi`(nama_div varchar(50), id_User int(11))
BEGIN
	DECLARE cekNamaDiv varchar(50);
    DECLARE namaBaru varchar(50);
    DECLARE IdDivisi int(11);
    DECLARE update_by int(11);
    DECLARE deleteStatus date;
    DECLARE hasil varchar(1000);
    
    SELECT nama_div INTO namaBaru;
    SELECT Nama_Divisi from divisi where divisi.Nama_Divisi like namaBaru into cekNamaDiv;
    SELECT Id_Divisi from divisi where divisi.Nama_Divisi like namaBaru into IdDivisi;
    SELECT id_User into update_by;
    if cekNamaDiv NOT like null or cekNamaDiv NOT like ""
    THEN
		SELECT Delete_at from divisi where divisi.Nama_Divisi like namaBaru into deleteStatus;
        IF deleteStatus is not null
        THEN
			UPDATE divisi 
				SET	Delete_at = NULL, Create_at = curdate(), Update_at = curdate()
                WHERE divisi.Nama_Divisi like namaBaru;
                select "Berhasil menambah divisi lama." as kesimpulan, deleteStatus;
                select concat("Tambah Divisi lama dengan ID Divisi :", IdDivisi , " dengan nama baru : " , namaBaru ,".") INTO HASIL;
                -- insert history --
				call `InsertHistory`(update_by, hasil);
                -- call SP Insert Divisi_departemen -- 
		ELSE
                select "Divisi sudah ada." as kesimpulan,deleteStatus;
		END IF;
    else
        INSERT into bts.divisi(Nama_Divisi, Create_at) VALUES 
		(namaBaru, curdate());
		select "Berhasil menambah divisi baru" as kesimpulan;
        select concat("Tambah Divisi baru dengan nama : " , namaBaru ,".") INTO HASIL;
		-- insert history --
		call `InsertHistory`(update_by, hasil);
        -- call SP update Divisi_departemen -- 
        END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `InsertEvaluasi` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `InsertEvaluasi`(id_nip varchar(20), id_user int, id_Stat int(11), Tanggal_Msk date, masaKerja date, Tanggal_Eval date, Tgl_Resign date)
BEGIN
	DECLARE adaEvalNIP int(11);
    DECLARE namaStatKaryawan varchar(50);
    DECLARE namaKaryawan varchar(300);
    DECLARE keteranganUpdate varchar(200);
    DECLARE hasil varchar(1000);
    
    select Id_Evaluasi into adaEvalNIP FROM bts.evaluasi WHERE bts.evaluasi.NIP like id_nip;
    select "Data yang diubah :" into keteranganUpdate;
    
    IF adaEvalNIP is not null
    THEN
		-- UPDATE YG UDH ADA--
		IF Tanggal_Msk is not null
        THEN
			UPDATE evaluasi
			SET Tanggal_Masuk = Tanggal_Msk ,Update_at = curdate()
			WHERE evaluasi.Id_Evaluasi like adaEvalNIP;
		 select concat(keteranganUpdate, "Tanggal Masuk , ") INTO keteranganUpdate;
		 select concat("Update tanggal masuk karyawan dengan NIP :", id_nip , " dengan tanggal masuk baru : " , Tanggal_Msk ,".") INTO HASIL;
		 -- insert history --
		 call `InsertHistory`(id_user, hasil);
        END IF;
        
        IF masaKerja is not null
        THEN
			UPDATE evaluasi
			SET Masa_kerja = masaKerja , Tanggal_Evaluasi =  concat(YEAR(masaKerja),'-',MONTH(masaKerja),'-',DAY(masaKerja)+1), Update_at = curdate()
			WHERE evaluasi.Id_Evaluasi like adaEvalNIP;
		 select concat(keteranganUpdate, "Masa Kerja , ") INTO keteranganUpdate;
		 select concat("Update masa kerja karyawan dengan NIP :", id_nip , " dengan tanggal  masa kerja : " , masaKerja ,".") INTO HASIL;
		 -- insert history --
		 call `InsertHistory`(id_user, hasil);
        END IF;
        
        IF Tanggal_Eval is not null
        THEN
			UPDATE evaluasi
			SET Tanggal_Evaluasi = Tanggal_Eval ,Update_at = curdate()
			WHERE evaluasi.Id_Evaluasi like adaEvalNIP;
		 select concat(keteranganUpdate, "Tanggal Evaluasi , ") INTO keteranganUpdate;
		 select concat("Update tanggal evaluasi karyawan dengan NIP :", id_nip , " dengan tanggal evaluasi baru : " , Tanggal_Eval ,".") INTO HASIL;
		 -- insert history --
		 call `InsertHistory`(id_user, hasil);
        END IF;
        
        IF id_Stat is not null
        THEN
			select Nama_Status into namaStatKaryawan FROM status_karyawan WHERE status_karyawan.Id_Status = id_Stat;
            IF namaStatKaryawan is not null
            THEN 
				UPDATE evaluasi
				SET Id_Status = id_Stat ,Update_at = curdate()
				WHERE evaluasi.Id_Evaluasi like adaEvalNIP;
                
                select concat(keteranganUpdate, "Status Karyawan , ") INTO keteranganUpdate;
				select concat("Update status karyawan dengan NIP :", id_nip , " dengan status baru : " , namaStatKaryawan ,".") INTO HASIL;
				-- insert history --
				call `InsertHistory`(id_user, hasil);
            ELSE
				SELECT concat('Tidak ada Status karyawan dengan ID Status : ', id_Stat, '. Gagal Update Evaluasi.') as kesimpulan;
            END IF;
        END IF;
        
		IF Tgl_Resign is not null
        THEN
			UPDATE evaluasi
			SET Tanggal_Resign = Tgl_Resign ,Update_at = curdate()
			WHERE evaluasi.Id_Evaluasi like adaEvalNIP;
		 select concat(keteranganUpdate, "Tanggal Resign , ") INTO keteranganUpdate;
		 select concat("Update tanggal resign karyawan dengan NIP :", id_nip , " dengan tanggal resign baru : " , Tgl_Resign ,".") INTO HASIL;
		 -- insert history --
		 call `InsertHistory`(id_user, hasil);
        END IF;
        Select * , "UPDATED SUCCESSFULLY" as kesimpulan, keteranganUpdate FROM evaluasi WHERE evaluasi.NIP like id_nip;
    ELSE
		SELECT Nama INTO namaKaryawan FROM karyawan WHERE karyawan.NIP = id_nip;
        IF namaKaryawan IS NOT NULL
        THEN
			INSERT into bts.evaluasi(Tanggal_Masuk, Masa_kerja, Tanggal_Evaluasi, Id_Status, Tanggal_Resign, Create_at, NIP) VALUES 
			(Tanggal_Msk, masaKerja, Tanggal_Eval, id_Stat, Tgl_Resign, CURDATE(),id_nip);
			select concat("Tambah Jabatan baru dengan nama : " , namaBaru ,".") INTO HASIL;
			-- insert history --
			call `InsertHistory`(update_by, hasil);
			Select * , "INSERT SUCCESSFULLY" as kesimpulan, keteranganUpdate FROM evaluasi WHERE evaluasi.NIP like id_nip;
        ELSE
			SELECT concat('Tidak ada Karyawan dengan NIP :', id_nip, 'Evaluasi gagal ditambahkan.' ) as kesimpulan;
        END IF;
    END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `InsertHistory` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `InsertHistory`(created_by int(11), ket varchar(1000))
BEGIN
	INSERT into bts.log (Keterangan, Create_at, Create_by) VALUES 
    (ket, curdate(), created_by);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `InsertJabatan` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `InsertJabatan`(nama_jab varchar(50), id_User int(11), id_Departemen int(11), lev int(11))
BEGIN
	DECLARE cekNamaJabatan varchar(50);
    DECLARE namaBaru varchar(50);
    DECLARE IdJabatan int(11);
    DECLARE update_by int(11);
    DECLARE IdDepart int(11);
    DECLARE low int(11);
    DECLARE eksisDepartemen int(11);
    DECLARE deleteStatus date;
    DECLARE hasil varchar(1000);
    
    SELECT nama_jab INTO namaBaru;
    SELECT Nama_Jabatan from jabatan where jabatan.Nama_Jabatan like namaBaru into cekNamaJabatan;
    SELECT Id_Jabatan from jabatan where jabatan.Nama_Jabatan like namaBaru into IdJabatan;
    SELECT id_User into update_by;
    SELECT id_Departemen INTO IdDepart;
	SELECT lev INTO low;
    SELECT Id_Departemen from departemen where departemen.Id_Departemen = IdDepart into eksisDepartemen;
    
    if cekNamaJabatan NOT like null or cekNamaJabatan NOT like ""
    THEN
		SELECT Delete_at from jabatan where jabatan.Nama_Jabatan like namaBaru into deleteStatus;
        IF deleteStatus is not null
        THEN
			IF eksisDepartemen is not null
            THEN
				UPDATE jabatan 
				SET	Delete_at = NULL, Create_at = curdate(), Update_at = curdate(), Id_Departemen = IdDepart, Level = low
                WHERE jabatan.Nama_Jabatan like namaBaru;
                select "Berhasil menambah jabatan lama." as kesimpulan, deleteStatus;
                select concat("Tambah jabatan lama dengan ID Jabatan :", IdJabatan , " dengan nama baru : " , namaBaru ,".") INTO HASIL;
                -- insert history --
				call `InsertHistory`(update_by, hasil);
                -- call SP Insert Divisi_departemen -- 
            ELSE
				select "Gagal menambah Jabatan. Karena Id Departemen salah (tidak ada)." as kesimpulan;
            END IF;
		ELSE
                select "Jabatan sudah ada." as kesimpulan,deleteStatus;
		END IF;
    else
        IF eksisDepartemen is not null
		THEN
			INSERT into bts.jabatan(Nama_Jabatan, Create_at, Id_Departemen, Level) VALUES 
			(namaBaru, curdate(),IdDepart, low);
			select "Berhasil menambah jabatan baru" as kesimpulan;
			select concat("Tambah Jabatan baru dengan nama : " , namaBaru ,".") INTO HASIL;
			-- insert history --
			call `InsertHistory`(update_by, hasil);
			-- call SP update Divisi_departemen -- 
        ELSE
			select "Gagal menambah Jabatan. Karena Id Departemen salah (tidak ada)." as kesimpulan;
        END IF;
	END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `insertKaryawan` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `insertKaryawan`(NIP varchar(20),
Nama varchar(300),
email varchar(100), 
Tempat_Lahir varchar(50), 
Tanggal_Lahir date, 
Alamat varchar(250), 
NPWP varchar(200),
BPJS_TenagaKerja varchar(250), 
BPJS_Kesehatan varchar(250) ,
Create_at date ,
Foto varchar(500) ,
Id_Divisi int(11) ,
Id_Departemen int(11) ,
Id_Jabatan int(11) ,
Alamat_Kerja varchar(250) ,
Telp_Kerja varchar(200),
Create_by int)
BEGIN
	declare count int;-- 0 berlum ada, 1 ada
    declare id int;
    
    select
		count(nip) into count
	from
		karyawan
	where
		karyawan.nip like nip;
        
	if count = 0
    then
		-- insert karyawan
		insert into karyawan (NIP, Nama, email, Tempat_Lahir , Tanggal_Lahir,  Alamat, NPWP, BPJS_TenagaKerja, BPJS_Kesehatan,
		Create_at ,Foto ,Id_Divisi,Id_Departemen,Id_Jabatan ,Alamat_Kerja ,Telp_Kerja) 
        values (NIP, Nama, email, Tempat_Lahir , Tanggal_Lahir,  Alamat, NPWP, BPJS_TenagaKerja, BPJS_Kesehatan,
		Create_at ,Foto ,Id_Divisi,Id_Departemen,Id_Jabatan ,Alamat_Kerja ,Telp_Kerja);
		-- insert log karyawan
        insert into log(Keterangan,Create_at,Create_by,NIP)
        values ('Memasukkan data karyawan','2019-07-29',create_by,NIP);
        -- insert user
        insert into user (email,password,Role,Create_at,Id_Karyawan)
        values (email,'password',1,'2019-07-29',NIP);
        
        select
			User.Id_User into id
		from 
			User
		Where
			User.email like email;
        
        -- insert log user
        insert into log(Keterangan,Create_at,Create_by,Id_User)
        values ('Memasukkan data User','2019-07-29',create_by,id);
    end if;
	select count;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `InsertProyekKaryawan` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `InsertProyekKaryawan`(idKar varchar(20), idPro int(11),id_User int(11))
BEGIN
	DECLARE existProyekKaryawan int(11);
    DECLARE hasil varchar(1000);
    
	SELECT Id_Proyek_Karyawan into existProyekKaryawan from proyek_karyawan where proyek_karyawan.Id_Karyawan = idKar and proyek_karyawan.Id_Proyek = idPro;
    
    IF existProyekKaryawan is null
    THEN
		INSERT INTO proyek_karyawan (Id_Karyawan, Id_Proyek, Create_at) VALUES (idKar,idPro, curdate());
        select concat("Tambah relasi proyek_karyawan dengan ID Proyek :", idPro , " dengan ID Karyawan : " , idKar ,".") INTO HASIL;
        -- insert history --
		call `InsertHistory`(id_User, hasil);
        select "Berhasil menambah relasi proyek_karyawan yang baru." as kesimpulan;
    ELSE
		SELECT concat("Id Proyek Karyawan dengan Id Karyawan : ", idKar ,
        " dan Id Proyek : ", idPro , " sudah ada relasi Proyek_Karyawan dengan ID Proyek_Karyawan : ", existProyekKaryawan, " .") as kesimpulan;
    END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `UpdateJabatan` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `UpdateJabatan`(nama_jab_Lama varchar(50), nama_jab_Baru varchar(50), id_User int(11), id_Depart int(11), lev int(11))
BEGIN
	DECLARE cekNamaJabatan int(11);
    DECLARE deleteStatus date;
    DECLARE eksisDepartemen int(11);
    DECLARE update_by int(11);
    DECLARE hasil varchar(1000);
    DECLARE kesimpulan varchar(200);
    
    -- PADA UPDATE jABATAN INI PATOKANNYA PADA ID JABATAN. BUKAN PADA NAMA jABATAN, KARENA NAMA JABATAN KEMUNGKINAN BISA DIUPDATE JUGA--
    SELECT Id_Jabatan  INTO cekNamaJabatan from jabatan WHERE Nama_Jabatan = nama_jab_Lama;
	SELECT Id_Departemen from departemen where departemen.Id_Departemen = id_Depart into eksisDepartemen;
    SELECT id_User into update_by;
    SELECT "Berhasil merubah : " INTO kesimpulan;
    
    IF cekNamaJabatan is not null
    THEN
		SELECT Delete_at from jabatan where jabatan.Nama_Jabatan = nama_jab_Lama into deleteStatus;
        IF deleteStatus is null
        THEN
			IF nama_jab_Baru IS NOT NULL
            THEN
				UPDATE jabatan 
				SET Nama_Jabatan = nama_jab_Baru , Update_at = curdate()
                WHERE jabatan.Id_Jabatan = cekNamaJabatan;
                select CONCAT(kesimpulan ," Nama Jabatan, ") into kesimpulan;
                select concat("Update Jabatan lama dengan ID Jabatan :", cekNamaJabatan , " dengan nama baru : " , nama_jab_Baru ,".") INTO HASIL;
                -- insert history --
				call `InsertHistory`(update_by, hasil);
                -- call SP Insert Divisi_departemen -- 
            END IF;
            IF id_Depart IS NOT NULL
            THEN
				IF eksisDepartemen IS NOT NULL
                THEN
					UPDATE jabatan 
					SET Id_Departemen = id_Depart , Update_at = curdate()
					WHERE jabatan.Id_Jabatan = cekNamaJabatan;
                    select CONCAT(kesimpulan ," ID Departemen pada Jabatan, ") into kesimpulan;
					select concat("Update Jabatan lama dengan ID Jabatan :", cekNamaJabatan , " dengan Id Departemen baru : " , id_Depart ,".") INTO HASIL;
					-- insert history --
					call `InsertHistory`(update_by, hasil);
					-- call SP Insert Divisi_departemen -- 
                END IF;
            END IF;
            IF lev IS NOT NULL
            THEN
				UPDATE jabatan 
				SET Level = lev , Update_at = curdate()
                WHERE jabatan.Id_Jabatan = cekNamaJabatan;
                select CONCAT(kesimpulan ," Level Jabatan, ") into kesimpulan;
                select concat("Update Jabatan lama dengan ID Jabatan :", cekNamaJabatan , " dengan Level baru : " , lev ,".") INTO HASIL;
                -- insert history --
				call `InsertHistory`(update_by, hasil);
                -- call SP Insert Divisi_departemen -- 
            END IF;
            select substring(kesimpulan, 1, length(kesimpulan)-2) into kesimpulan;
            select kesimpulan;
		ELSE
                select "Jabatan tidak ditemukan atau sudah dihapus." into kesimpulan;
                select kesimpulan;
		END IF;
    ELSE
		SELECT concat("Tidak ada Jabatan dengan nama : ", nama_jab_Lama) into kesimpulan;
        select kesimpulan;
    END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `UpdateKaryawan` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `UpdateKaryawan`(update_by int, idChange varchar(20), namaKaryawan varchar(300), email varchar(100),
 foto longtext, Tanggal_Lahir date, address varchar(250),
Tempat_Lahir varchar(50), NPWP varchar(200), BPJS_TenagaKerja varchar(250), BPJS_Kesehatan varchar(250),
Id_Jab INT(11), Alamat_Kerja varchar(250), Telp_Kerja varchar(200))
BEGIN
	DECLARE findKaryawan varchar(20);
    DECLARE findKaryawanAktive varchar(20);
    DECLARE keteranganUpdate varchar(200);
    DECLARE name_divisi varchar(50);
    DECLARE name_departemen varchar(50);
    DECLARE name_jabatan varchar(50);
    DECLARE hasil varchar(1000);
    
    
    select karyawan.NIP into findKaryawan from karyawan where karyawan.NIP like idChange;
    select "" into keteranganUpdate;
    -- tidak ada karyawan dengan nip tersebut--
    if findKaryawan = null
    THEN
		Select "Belum ada Karyawan dengan NIP : " + idChange;
	else
		 select karyawan.NIP into findKaryawanAktive from karyawan where bts.karyawan.NIP like idChange and bts.karyawan.Delete_at is null;
        -- ada karyawan dengan nip tsb dan masih aktive--
		if findKaryawanAktive is not null
        THEN
			if namaKaryawan IS NOT NULL
			THEN UPDATE karyawan
				SET Nama = namaKaryawan ,Update_at = curdate()
				WHERE karyawan.NIP like idChange;
			
            select concat(keteranganUpdate, "Nama Karyawan , ") INTO keteranganUpdate;
            select concat("Update nama karyawan lama dengan NIP :", idChange , " dengan nama baru : " , namaKaryawan ,".") INTO HASIL;
            -- insert history --
            call `InsertHistory`(update_by, hasil);
			END IF;
			
			if email IS NOT NULL
			THEN UPDATE karyawan
					SET karyawan.email = email , Update_at = curdate()
					WHERE bts.karyawan.NIP like idChange;
			select concat(keteranganUpdate, "E-mail , ") into keteranganUpdate;
            -- insert history --
            select concat("Update email karyawan lama dengan NIP :",idChange," dengan email baru : ",email,".") into hasil;
			call `InsertHistory`(update_by, hasil);
			END IF;        
			
			if foto IS NOT NULL
				THEN 
					UPDATE karyawan
					SET Foto = foto , Update_at = curdate()
					WHERE karyawan.NIP like idChange;
			select concat(keteranganUpdate, "Foto , ") INTO keteranganUpdate;
            select concat("Update foto karyawan lama dengan NIP :",idChange," dengan foto baru : ",foto," .") into hasil;
            -- insert history --
            call `InsertHistory`(update_by, hasil);
			END IF; 
            
            if Tanggal_Lahir IS NOT NULL
				THEN 
					UPDATE karyawan
					SET Tanggal_Lahir = Tanggal_Lahir , Update_at = curdate()
					WHERE karyawan.NIP like idChange;
			select concat(keteranganUpdate, "Tanggal Lahir , ") INTO keteranganUpdate;
            select concat("Update tanggal lahir karyawan lama dengan NIP :",idChange," dengan tanggal lahir baru : ",Tanggal_Lahir,".") into hasil;
            -- insert history --
            call `InsertHistory`(update_by, hasil);
			END IF;  
            
            if address IS NOT NULL
				THEN UPDATE karyawan
					SET Alamat = address , Update_at = curdate()
					WHERE karyawan.NIP like idChange;
            select concat(keteranganUpdate, "Alamat , ") INTO keteranganUpdate;
            select concat("Update alamat karyawan lama dengan NIP :",idChange," dengan alamat baru : ",address," .") into hasil;
            -- insert history --
            call `InsertHistory`(update_by, hasil);
			END IF;
            
            if Tempat_Lahir IS NOT NULL
				THEN UPDATE karyawan
					SET Tempat_Lahir = Tempat_Lahir , Update_at = curdate()
					WHERE karyawan.NIP like idChange;
            select concat(keteranganUpdate, "Tempat Lahir , ") INTO keteranganUpdate;
			select concat("Update tempat lahir karyawan lama dengan NIP :",idChange," dengan tempat lahir baru : ",Tempat_Lahir," .") into hasil;
            -- insert history --
            call `InsertHistory`(update_by, hasil);

			END IF;
            
            if NPWP IS NOT NULL
				THEN UPDATE karyawan
					SET NPWP = NPWP , Update_at = curdate()
					WHERE karyawan.NIP like idChange;
            select concat(keteranganUpdate, "NPWP , ") INTO keteranganUpdate;
            select concat("Update NPWP karyawan lama dengan NIP :",idChange," dengan NPWP baru : ",NPWP," .") into hasil;
            -- insert history --
           call `InsertHistory`(update_by, hasil);
			END IF;
            
            if BPJS_TenagaKerja IS NOT NULL
				THEN UPDATE karyawan
					SET BPJS_TenagaKerja = BPJS_TenagaKerja , Update_at = curdate()
					WHERE karyawan.NIP like idChange;
            select concat(keteranganUpdate, "BPJS Tenaga Kerja , ") INTO keteranganUpdate;
            select concat("Update BPJS Tenaga Kerja karyawan lama dengan NIP :",idChange," dengan BPJS Tenaga Kerja baru : ",BPJS_TenagaKerja," .") into hasil;
            -- insert history --
            call `InsertHistory`(update_by, hasil);
			END IF;
            
            if BPJS_Kesehatan IS NOT NULL
				THEN UPDATE karyawan
					SET BPJS_Kesehatan = BPJS_Kesehatan , Update_at = curdate()
					WHERE karyawan.NIP like idChange;
            select concat(keteranganUpdate, "BPJS Kesehatan , ") INTO keteranganUpdate;
            select concat("Update BPJS Kesehatan karyawan lama dengan NIP :",idChange," dengan BPJS Kesehatan baru : ",BPJS_Kesehatan," .") into hasil;
            -- insert history --
            call `InsertHistory`(update_by, hasil);
			END IF;
            
            -- if Id_Div IS NOT NULL
			--	THEN UPDATE karyawan
					-- SET Id_Divisi = Id_Div , Update_at = curdate()
					-- WHERE karyawan.NIP like idChange;
          --  select concat(keteranganUpdate, "Id Divisi , ") INTO keteranganUpdate;
          --  select Nama_Divisi into name_divisi from divisi where divisi.Id_Divisi = Id_Div;
         --   select concat("Update Id Divisi karyawan lama dengan NIP :",idChange," dipindahkan ke Divisi : ",name_divisi," .") into hasil;
            -- insert history --
        --   call `InsertHistory`(update_by, hasil);
		--	-- END IF;
            
          --  if Id_Depart IS NOT NULL
				-- THEN UPDATE karyawan
					-- SET Id_Departemen = Id_Depart , Update_at = curdate()
					-- WHERE karyawan.NIP like idChange;
           -- select concat(keteranganUpdate, "Id Departemen , ") INTO keteranganUpdate;
          --  select Nama_Departemen into name_departemen from departemen where departemen.Id_Departemen = Id_Depart;
           -- select concat("Update Id Departemen karyawan lama dengan NIP :",idChange," dipindahkan ke Departemen : ",name_departemen," .") into hasil;
            -- insert history --
          --  call `InsertHistory`(update_by, hasil);

			-- END IF;
            
            if Id_Jab IS NOT NULL
				THEN UPDATE karyawan
					SET Id_Jabatan = Id_Jab , Update_at = curdate()
					WHERE karyawan.NIP like idChange;
            select concat(keteranganUpdate, "Id Jabatan , ") INTO keteranganUpdate;
            select Nama_Jabatan into name_jabatan from jabatan where jabatan.Id_Jabatan = Id_Jab;
            select concat("Update Id jabatan karyawan lama dengan NIP :",idChange," dipindahkan ke Jabatan : ",name_jabatan," .") into hasil;
            -- insert history --
            call `InsertHistory`(update_by, hasil);
			END IF;
            
            if Alamat_Kerja IS NOT NULL
				THEN UPDATE karyawan
					SET Alamat_Kerja = Alamat_Kerja , Update_at = curdate()
					WHERE karyawan.NIP like idChange;
            select concat(keteranganUpdate, "Alamat Kerja , ") INTO keteranganUpdate;
            select concat("Update Alamat kerja karyawan lama dengan NIP :",idChange," dengan Alamat Kerja baru : ",Alamat_Kerja," .") into hasil;
             -- insert history --
            call `InsertHistory`(update_by, hasil);
			END IF;
			
            if Telp_Kerja IS NOT NULL
				THEN UPDATE karyawan
					SET Telp_Kerja = Telp_Kerja , Update_at = curdate()
					WHERE karyawan.NIP like idChange;
            select concat(keteranganUpdate, "Telp Kerja , ") INTO keteranganUpdate;
            select concat("Update Telp kerja karyawan lama dengan NIP :",idChange," dengan Nomor Telpon Kerja baru : ",Telp_Kerja," .") into hasil;
            -- insert history --
            call `InsertHistory`(update_by, hasil);
			END IF;
			Select * , "UPDATED SUCCESSFULLY" as statsu, keteranganUpdate FROM karyawan WHERE karyawan.NIP like idChange;
	   else
			select concat("Karyawan dengan NIP : ",idChange," Sudah tidak begabung dalam Perusahaan.") into hasil;
			Select * , "UPDATE FAILED" as statsu, hasil FROM karyawan WHERE karyawan.NIP like idChange;
       END IF;
	END IF;   
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-08-20 13:34:04
