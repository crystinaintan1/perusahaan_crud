CREATE DATABASE  IF NOT EXISTS `bts` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `bts`;
-- MySQL dump 10.13  Distrib 8.0.17, for Win64 (x86_64)
--
-- Host: localhost    Database: bts
-- ------------------------------------------------------
-- Server version	8.0.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `departemen`
--

DROP TABLE IF EXISTS `departemen`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `departemen` (
  `Id_Departemen` int(11) NOT NULL AUTO_INCREMENT,
  `Nama_Departemen` varchar(50) NOT NULL,
  `Create_at` date NOT NULL,
  `Update_at` date DEFAULT NULL,
  `Delete_at` date DEFAULT NULL,
  `Id_Divisi` int(11) NOT NULL,
  PRIMARY KEY (`Id_Departemen`),
  KEY `Id_Divisi5_idx` (`Id_Divisi`),
  CONSTRAINT `Id_Divisi5` FOREIGN KEY (`Id_Divisi`) REFERENCES `divisi` (`Id_Divisi`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `departemen`
--

LOCK TABLES `departemen` WRITE;
/*!40000 ALTER TABLE `departemen` DISABLE KEYS */;
INSERT INTO `departemen` VALUES (4,'A','2019-08-02',NULL,NULL,4),(5,'Teuku Hashrul','2019-08-02','2019-08-02',NULL,4);
/*!40000 ALTER TABLE `departemen` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `divisi`
--

DROP TABLE IF EXISTS `divisi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `divisi` (
  `Id_Divisi` int(11) NOT NULL AUTO_INCREMENT,
  `Nama_Divisi` varchar(50) NOT NULL,
  `Create_at` date NOT NULL,
  `Update_at` date DEFAULT NULL,
  `Delete_at` date DEFAULT NULL,
  PRIMARY KEY (`Id_Divisi`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `divisi`
--

LOCK TABLES `divisi` WRITE;
/*!40000 ALTER TABLE `divisi` DISABLE KEYS */;
INSERT INTO `divisi` VALUES (4,'A','2019-08-02',NULL,NULL);
/*!40000 ALTER TABLE `divisi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `evaluasi`
--

DROP TABLE IF EXISTS `evaluasi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `evaluasi` (
  `Id_Evaluasi` int(11) NOT NULL AUTO_INCREMENT,
  `Tanggal_Masuk` date NOT NULL,
  `Masa_kerja` date DEFAULT NULL,
  `Tanggal_Evaluasi` date DEFAULT NULL,
  `Id_Status` int(11) NOT NULL,
  `Tanggal_Resign` date DEFAULT NULL,
  `Create_at` date NOT NULL,
  `Update_at` date DEFAULT NULL,
  `Delete_at` date DEFAULT NULL,
  `NIP` varchar(20) NOT NULL,
  PRIMARY KEY (`Id_Evaluasi`),
  KEY `NIP1_idx` (`NIP`),
  CONSTRAINT `NIP1` FOREIGN KEY (`NIP`) REFERENCES `karyawan` (`NIP`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `evaluasi`
--

LOCK TABLES `evaluasi` WRITE;
/*!40000 ALTER TABLE `evaluasi` DISABLE KEYS */;
/*!40000 ALTER TABLE `evaluasi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jabatan`
--

DROP TABLE IF EXISTS `jabatan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `jabatan` (
  `Id_Jabatan` int(11) NOT NULL AUTO_INCREMENT,
  `Nama_Jabatan` varchar(50) NOT NULL,
  `Create_at` date NOT NULL,
  `Update_at` date DEFAULT NULL,
  `Delete_at` date DEFAULT NULL,
  `Id_Departemen` int(11) NOT NULL,
  `Level` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id_Jabatan`),
  KEY `Id_Departemen4_idx` (`Id_Departemen`),
  CONSTRAINT `Id_Departemen4` FOREIGN KEY (`Id_Departemen`) REFERENCES `departemen` (`Id_Departemen`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jabatan`
--

LOCK TABLES `jabatan` WRITE;
/*!40000 ALTER TABLE `jabatan` DISABLE KEYS */;
INSERT INTO `jabatan` VALUES (4,'A','2019-08-02',NULL,NULL,4,0);
/*!40000 ALTER TABLE `jabatan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `karyawan`
--

DROP TABLE IF EXISTS `karyawan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `karyawan` (
  `NIP` varchar(20) NOT NULL,
  `Nama` varchar(300) NOT NULL,
  `NIK` varchar(30) NOT NULL,
  `email` varchar(100) NOT NULL,
  `Tempat_Lahir` varchar(50) NOT NULL,
  `Tanggal_Lahir` date NOT NULL,
  `Alamat` varchar(250) NOT NULL,
  `NPWP` varchar(200) DEFAULT NULL,
  `BPJS_TenagaKerja` varchar(250) NOT NULL,
  `BPJS_Kesehatan` varchar(250) NOT NULL,
  `Create_at` date NOT NULL,
  `Update_at` date DEFAULT NULL,
  `Delete_at` date DEFAULT NULL,
  `Foto` longtext NOT NULL,
  `Id_Jabatan` int(11) NOT NULL,
  `Alamat_Kerja` varchar(250) NOT NULL,
  `Telp_Kerja` varchar(200) NOT NULL,
  PRIMARY KEY (`NIP`),
  KEY `Id_Jabatan1_idx` (`Id_Jabatan`),
  CONSTRAINT `Id_Jabatan1` FOREIGN KEY (`Id_Jabatan`) REFERENCES `jabatan` (`Id_Jabatan`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `karyawan`
--

LOCK TABLES `karyawan` WRITE;
/*!40000 ALTER TABLE `karyawan` DISABLE KEYS */;
INSERT INTO `karyawan` VALUES ('111','Hashrul','12334523423432','hash54sdds@gmail.com','Kasur','1990-08-12','Jalan Raya','2eaefew3d','3432dwqsds','3r4wf454wr','2019-07-19',NULL,NULL,'fwsdgbsrf43q3da',4,'Jalan','029282772'),('11345451','Hashrul','12334523423432','hash54sdds@gmail.com','Kasur','1990-08-12','Jalan Raya','2eaefew3d','3432dwqsds','3r4wf454wr','2019-07-19',NULL,NULL,'fwsdgbsrf43q3da',4,'Jalan','029282772');
/*!40000 ALTER TABLE `karyawan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `log`
--

DROP TABLE IF EXISTS `log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `log` (
  `Id_Log` int(11) NOT NULL AUTO_INCREMENT,
  `Keterangan` varchar(1000) NOT NULL,
  `Create_at` date NOT NULL,
  `Create_by` int(11) NOT NULL,
  PRIMARY KEY (`Id_Log`)
) ENGINE=InnoDB AUTO_INCREMENT=109 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `log`
--

LOCK TABLES `log` WRITE;
/*!40000 ALTER TABLE `log` DISABLE KEYS */;
INSERT INTO `log` VALUES (88,'Memasukkan data karyawan dengan NIP : 111','2019-08-02',1),(89,'Memasukkan data User dengan Id_User : 21','2019-08-02',1),(90,'Memasukkan data karyawan dengan NIP : 111','2019-08-02',1),(91,'Memasukkan data User dengan Id_User : 22','2019-08-02',1),(92,'Memasukkan data karyawan dengan NIP : 111','2019-08-02',1),(93,'Memasukkan data User dengan Id_User : 23','2019-08-02',1),(94,'Menambahkan Departemen dengan Id_Departemen : 5','2019-08-02',1),(95,'Update nama Departemen dengan Id_Departemen : 5','2019-08-02',1),(96,'Delete Departemen dengan Id_Departemen : 5','2019-08-02',1),(97,'Update Departemen (delete_at) dengan Id_Departemen : 5','2019-08-02',1),(98,'Menambahkan Status Karyawan dengan Id_Status : 4','2019-08-02',1),(99,'Menambahkan Status Karyawan dengan Id_Status : 7','2019-08-03',1),(100,'Update Status Karyawan dengan Id_Status : 7','2019-08-03',1),(101,'Update Statuse Karyawan dengan Id_Status : 7','2019-08-03',1),(102,'Update Statuse Karyawan dengan Id_Status : 7','2019-08-03',1),(103,'menambahkan Todo List dengan Id_Task : 1','2019-08-03',1),(104,'menambahkan Todo List dengan Id_Task : 2','2019-08-03',1),(105,'Update todo list dengan Id_Task : 2','2019-08-03',1),(106,'menambahkan Todo List dengan Id_Task : 3','2019-08-03',1),(107,'Delete Todo List dengan Id_Task : 2','2019-08-03',1),(108,'Memasukkan data karyawan dengan NIP : 11345451','2019-08-05',1);
/*!40000 ALTER TABLE `log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proyek`
--

DROP TABLE IF EXISTS `proyek`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `proyek` (
  `Id_Proyek` int(11) NOT NULL AUTO_INCREMENT,
  `Nama_Proyek` varchar(100) NOT NULL,
  `Tanggal_Mulai` date NOT NULL,
  `Tanggal_Selesai` date NOT NULL,
  `Status` varchar(20) NOT NULL,
  `Klien` varchar(50) NOT NULL,
  `kategori` varchar(50) NOT NULL,
  `Create_at` date NOT NULL,
  `Update_at` date DEFAULT NULL,
  `Delete_at` date DEFAULT NULL,
  PRIMARY KEY (`Id_Proyek`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proyek`
--

LOCK TABLES `proyek` WRITE;
/*!40000 ALTER TABLE `proyek` DISABLE KEYS */;
/*!40000 ALTER TABLE `proyek` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proyek_karyawan`
--

DROP TABLE IF EXISTS `proyek_karyawan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `proyek_karyawan` (
  `Id_Proyek_Karyawan` int(11) NOT NULL AUTO_INCREMENT,
  `Id_Karyawan` varchar(20) NOT NULL,
  `Id_Proyek` int(11) NOT NULL,
  `Create_at` date NOT NULL,
  `Update_at` date DEFAULT NULL,
  `Delete_at` date DEFAULT NULL,
  PRIMARY KEY (`Id_Proyek_Karyawan`),
  KEY `Id_Karyawan1_idx` (`Id_Karyawan`),
  KEY `Id_Proyek1_idx` (`Id_Proyek`),
  CONSTRAINT `Id_Karyawan1` FOREIGN KEY (`Id_Karyawan`) REFERENCES `karyawan` (`NIP`),
  CONSTRAINT `Id_Proyek1` FOREIGN KEY (`Id_Proyek`) REFERENCES `proyek` (`Id_Proyek`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proyek_karyawan`
--

LOCK TABLES `proyek_karyawan` WRITE;
/*!40000 ALTER TABLE `proyek_karyawan` DISABLE KEYS */;
/*!40000 ALTER TABLE `proyek_karyawan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rekening`
--

DROP TABLE IF EXISTS `rekening`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `rekening` (
  `Id_Rekening` int(11) NOT NULL AUTO_INCREMENT,
  `Nomor_Rekening` int(11) DEFAULT NULL,
  `Create_at` date NOT NULL,
  `Update_at` date DEFAULT NULL,
  `Delete_at` date DEFAULT NULL,
  `NIP` varchar(20) NOT NULL,
  PRIMARY KEY (`Id_Rekening`),
  KEY `NIP_idx` (`NIP`),
  CONSTRAINT `NIP` FOREIGN KEY (`NIP`) REFERENCES `karyawan` (`NIP`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rekening`
--

LOCK TABLES `rekening` WRITE;
/*!40000 ALTER TABLE `rekening` DISABLE KEYS */;
/*!40000 ALTER TABLE `rekening` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `status_karyawan`
--

DROP TABLE IF EXISTS `status_karyawan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `status_karyawan` (
  `Id_Status` int(11) NOT NULL AUTO_INCREMENT,
  `Nama_Status` varchar(50) NOT NULL,
  `Create_at` date NOT NULL,
  `Update_at` date DEFAULT NULL,
  `Delete_at` date DEFAULT NULL,
  PRIMARY KEY (`Id_Status`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `status_karyawan`
--

LOCK TABLES `status_karyawan` WRITE;
/*!40000 ALTER TABLE `status_karyawan` DISABLE KEYS */;
INSERT INTO `status_karyawan` VALUES (4,'Tetap','2019-08-02',NULL,NULL),(7,'Update','2019-08-03','2019-08-03',NULL);
/*!40000 ALTER TABLE `status_karyawan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `todo_list`
--

DROP TABLE IF EXISTS `todo_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `todo_list` (
  `Id_Task` int(11) NOT NULL AUTO_INCREMENT,
  `Nama_Task` varchar(50) NOT NULL,
  `Deskripsi` varchar(250) NOT NULL,
  `Status_Task` varchar(20) NOT NULL,
  `Create_at` date NOT NULL,
  `Update_at` date DEFAULT NULL,
  `Delete_at` date DEFAULT NULL,
  `Id_User` int(11) NOT NULL,
  PRIMARY KEY (`Id_Task`),
  KEY `Id_User_idx` (`Id_User`),
  CONSTRAINT `Id_User` FOREIGN KEY (`Id_User`) REFERENCES `user` (`Id_User`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `todo_list`
--

LOCK TABLES `todo_list` WRITE;
/*!40000 ALTER TABLE `todo_list` DISABLE KEYS */;
INSERT INTO `todo_list` VALUES (1,'Task','Deskripsi','Baru','2019-08-03',NULL,NULL,1),(2,'Update','Update','Update','2019-08-03','2019-08-03','2019-08-03',1),(3,'Task 123','Deskripsi','Baru','2019-08-03',NULL,NULL,1);
/*!40000 ALTER TABLE `todo_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user` (
  `Id_User` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(100) DEFAULT NULL,
  `Password` varchar(50) NOT NULL,
  `Role` tinyint(2) NOT NULL,
  `Create_at` date NOT NULL,
  `Update_at` date DEFAULT NULL,
  `Delete_at` date DEFAULT NULL,
  `Id_Karyawan` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`Id_User`),
  KEY `Id_karyawan_idx` (`Id_Karyawan`),
  CONSTRAINT `Id_karyawan` FOREIGN KEY (`Id_Karyawan`) REFERENCES `karyawan` (`NIP`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'hcm@bts.id','password',0,'2019-07-29',NULL,NULL,NULL),(19,'test@bts.id','Pass123',0,'2019-07-31',NULL,NULL,NULL),(23,'hash54sdds@gmail.com','password',1,'2019-08-02',NULL,NULL,'111'),(24,'hash54sdds@gmail.com','password',1,'2019-08-05',NULL,NULL,'11345451');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'bts'
--

--
-- Dumping routines for database 'bts'
--
/*!50003 DROP PROCEDURE IF EXISTS `deleteDepartemen` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `deleteDepartemen`(
Id_D int,
Create_by int)
BEGIN
	declare count int;
    select count(Id_Departemen) into count from departemen where Id_Departemen = Id_D and delete_at is null;
    if count = 1
    then
        update departemen set Update_at = curdate(), Delete_at = curdate() where Nama_Departemen like Nama;
        
        insert into log(Keterangan,Create_at,Create_by) values(concat("Delete Departemen dengan Id_Departemen : ",Id_D),curdate(),Create_by);
    end if;
    select count;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `deleteKaryawan` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `deleteKaryawan`(
NIP varchar(20),
Create_by int)
BEGIN
	declare id_u,id_r,id_e,id_pk,count int;
    
    select count(NIP) into count from karyawan where NIP like NIP and delete_at is null;
    
    if count = 1
    then

		update karyawan set Update_at = curdate(), delete_at = curdate() where NIP like NIP;
		update user set Update_at = curdate(), delete_at = curdate() where Id_Karyawan like NIP ;
		update rekening set Update_at = curdate(), delete_at = curdate() where NIP like NIP;
		update evaluasi set Update_at = curdate(), delete_at = curdate() where NIP like NIP;
		update proyek_karyawan set Update_at = curdate(), delete_at = curdate() where NIP like NIP;
		
		insert into log(Keterangan,Create_at,Create_by) values(concat('Delete karyawan dengan NIP : ',NIP),curdate(),Create_by);
		
		select Id_User into id_u from User where Id_Karyawan like NIP;
		insert into log (Keterangan,Create_at,Create_by) values(concat('Delete user dengan Id_User : ',id_u),curdate(),Create_by);
		
		select Id_Rekening into id_r from Rekening where NIP like NIP;
		insert into log (Keterangan,Create_at,Create_by) values(concat('Delete Rekening dengan Id_Rekening : ',id_r),curdate(),Create_by);
		
		select Id_Evaluasi into id_e from Evaluasi where NIP like NIP;
		insert into log (Keterangan,Create_at,Create_by) values(concat('Delete evaluasi dengan Id_Evaluasi : ',id_e),curdate(),Create_by);
		
		select Id_Proyek_Karyawan into id_pk from proyek_karyawan where NIP like NIP;
		insert into log (Keterangan,Create_by) values(concat('Delete Proyek Karyawan dengan Id_Proyek_Karyawan : ',id_pk),curdate(),Create_by);
	end if;
    
    select count;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `deleteProyek` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `deleteProyek`(
Id_P int,
Create_by int)
BEGIN
	declare count int;
    
    select count(Id_Proyek) into count from proyek where Id_Proyek = Id_P and delete_at is null;
    
    if count = 1
    then
		update proyek set Update_at = curdate(), Delete_at = curdate() where Id_Proyek = Id_P;
        
        insert into log(Keterangan,Create_at,Create_by) values(concat('Delete proyek dengan Id_Proyek : ',Id_P),curdate(),Create_by);
        
        update proyek_Karyawan set Delete_at = curdate() where Id_Proyek = Id_P;
        
        insert into log(Keterangan,Create_at,Create_by) values(concat('Delete proyek_karyawan dengan Id_Proyek : ',Id_P),curdate(),Create_by);
    end if;
    
    select count;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `deleteRekening` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `deleteRekening`(
Id_R int,
Create_by int)
BEGIN
	declare count int;
    
    select count(Id_Rekening) into count from rekening where Id_Rekening = Id_R and Delete_at is null;
    
    if count = 1
    then
		update rekening set Update_at = curdate(), Delete_at = curdate() where Id_Rekening = Id_R;
        
        insert into log(Keterangan,Create_at,Create_by) values (concat('Delete rekening dengan Id_Rekening : ',Id_R),curdate(),Create_by);
    end if;
    
    select count;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `deleteStatusKaryawan` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `deleteStatusKaryawan`(
Id_S int,
Create_by int)
BEGIN
	declare count int;
    
    select count(Id_Status) into count from status_karyawan where Id_Status = Id_s and Delete_at is null;
    
    if count = 1
    then
		update status_karyawan set Update_at = curdate(), Delete_at = curdate() where Id_Status = Id_S;
        
        insert into log(Keterangan,Create_at,Create_by) values(concat('Delete Status Karyawan dengan Id_Status : ',Id_S),curdate(),Create_by);
    end if;
    
    select count;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `deleteTodoList` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `deleteTodoList`(
Id_T int,
Create_by int)
BEGIN
	declare count int;
    
    select count(Id_Task) into count from todo_list where Id_Task = Id_T and Delete_at is null;
    
    if count = 1
    then
		update todo_list set Delete_at = curdate() where Id_Task = Id_T;
        
        insert into log(Keterangan,Create_at,Create_by) values(concat('Delete Todo List dengan Id_Task : ',Id_T),curdate(),Create_by);
    end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `deleteUser` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `deleteUser`(
Id_U int,
Create_by int)
BEGIN
	declare count int;
    
    select count(Id_User) into count from user where Id_User = Id_U and Delete_at is null;
    
    if count = 1
    then
		update user set Update_at = curdate(), delete_at = curdate() where Id_User = Id_U;
        
        insert into log(Keterangan,Create_at,Create_by) values (concat("Delete admin dengan Id_User : ",Id_U),curdate(),Create_by);
    end if;
    
    select count;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `insertDepartemen` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `insertDepartemen`(
Nama_Departemen_ varchar(50),
Create_by int,
Id_Divisi int)
BEGIN
	declare count,id_d int;
    
    select count(Id_Departemen) into count from departemen where Nama_Departemen like Nama_Departemen_ and Delete_at is not null;
    -- 0 belum ada, 1 udah ada delete_at =  null
    
    if count = 0
    then
		insert into departemen (Nama_Departemen,Create_at,Id_Divisi) values (Nama_Departemen_,curdate(),Id_Divisi);
        
        select Id_departemen into id_d from Departemen where Nama_Departemen like Nama_Departemen_;
        insert into log(Keterangan,Create_at,Create_by) values (concat('Menambahkan Departemen dengan Id_Departemen : ',id_d),curdate(),Create_by);
    else
		update departemen set Create_at = curdate(), Update_at=curdate(), Delete_at = null where Nama_Departemen like Nama_Departemen_;
        
        select Id_departemen into id_d from Departemen where Nama_Departemen like Nama_Departemen_;
        insert into log(Keterangan,Create_at,Create_by) values (concat('Update Departemen (delete_at) dengan Id_Departemen : ',id_d),curdate(),Create_by);
    end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `InsertDivisi` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `InsertDivisi`(nama_div varchar(50), id_User int(11))
BEGIN
	DECLARE cekNamaDiv varchar(50);
    DECLARE namaBaru varchar(50);
    DECLARE IdDivisi int(11);
    DECLARE update_by int(11);
    DECLARE deleteStatus date;
    DECLARE hasil varchar(1000);
    
    SELECT nama_div INTO namaBaru;
    SELECT Nama_Divisi from divisi where divisi.Nama_Divisi like namaBaru into cekNamaDiv;
    SELECT Id_Divisi from divisi where divisi.Nama_Divisi like namaBaru into IdDivisi;
    SELECT id_User into update_by;
    if cekNamaDiv NOT like null or cekNamaDiv NOT like ""
    THEN
		SELECT Delete_at from divisi where divisi.Nama_Divisi like namaBaru into deleteStatus;
        IF deleteStatus is not null
        THEN
			UPDATE divisi 
				SET	Delete_at = NULL, Create_at = curdate(), Update_at = curdate()
                WHERE divisi.Nama_Divisi like namaBaru;
                select "Berhasil menambah divisi lama." as kesimpulan, deleteStatus;
                select concat("Tambah Divisi lama dengan ID Divisi :", IdDivisi , " dengan nama baru : " , namaBaru ,".") INTO HASIL;
                -- insert history --
				call `InsertHistory`(update_by, hasil);
                -- call SP Insert Divisi_departemen -- 
		ELSE
                select "Divisi sudah ada." as kesimpulan,deleteStatus;
		END IF;
    else
        INSERT into bts.divisi(Nama_Divisi, Create_at) VALUES 
		(namaBaru, curdate());
		select "Berhasil menambah divisi baru" as kesimpulan;
        select concat("Tambah Divisi baru dengan nama : " , namaBaru ,".") INTO HASIL;
		-- insert history --
		call `InsertHistory`(update_by, hasil);
        -- call SP update Divisi_departemen -- 
        END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `InsertHistory` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `InsertHistory`(created_by int(11), ket varchar(1000))
BEGIN
	INSERT into bts.log (Keterangan, Create_at, Create_by) VALUES 
    (ket, curdate(), created_by);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `InsertJabatan` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `InsertJabatan`(nama_jab varchar(50), id_User int(11), id_Departemen int(11), lev int(11))
BEGIN
	DECLARE cekNamaJabatan varchar(50);
    DECLARE namaBaru varchar(50);
    DECLARE IdJabatan int(11);
    DECLARE update_by int(11);
    DECLARE IdDepart int(11);
    DECLARE low int(11);
    DECLARE eksisDepartemen int(11);
    DECLARE deleteStatus date;
    DECLARE hasil varchar(1000);
    
    SELECT nama_jab INTO namaBaru;
    SELECT Nama_Jabatan from jabatan where jabatan.Nama_Jabatan like namaBaru into cekNamaJabatan;
    SELECT Id_Jabatan from jabatan where jabatan.Nama_Jabatan like namaBaru into IdJabatan;
    SELECT id_User into update_by;
    SELECT id_Departemen INTO IdDepart;
	SELECT lev INTO low;
    SELECT Id_Departemen from departemen where departemen.Id_Departemen = IdDepart into eksisDepartemen;
    
    if cekNamaJabatan NOT like null or cekNamaJabatan NOT like ""
    THEN
		SELECT Delete_at from jabatan where jabatan.Nama_Jabatan like namaBaru into deleteStatus;
        IF deleteStatus is not null
        THEN
			IF eksisDepartemen is not null
            THEN
				UPDATE jabatan 
				SET	Delete_at = NULL, Create_at = curdate(), Update_at = curdate(), Id_Departemen = IdDepart, Level = low
                WHERE jabatan.Nama_Jabatan like namaBaru;
                select "Berhasil menambah jabatan lama." as kesimpulan, deleteStatus;
                select concat("Tambah jabatan lama dengan ID Jabatan :", IdJabatan , " dengan nama baru : " , namaBaru ,".") INTO HASIL;
                -- insert history --
				call `InsertHistory`(update_by, hasil);
                -- call SP Insert Divisi_departemen -- 
            ELSE
				select "Gagal menambah Jabatan. Karena Id Departemen salah (tidak ada)." as kesimpulan;
            END IF;
		ELSE
                select "Jabatan sudah ada." as kesimpulan,deleteStatus;
		END IF;
    else
        IF eksisDepartemen is not null
		THEN
			INSERT into bts.jabatan(Nama_Jabatan, Create_at, Id_Departemen, Level) VALUES 
			(namaBaru, curdate(),IdDepart, low);
			select "Berhasil menambah jabatan baru" as kesimpulan;
			select concat("Tambah Jabatan baru dengan nama : " , namaBaru ,".") INTO HASIL;
			-- insert history --
			call `InsertHistory`(update_by, hasil);
			-- call SP update Divisi_departemen -- 
        ELSE
			select "Gagal menambah Jabatan. Karena Id Departemen salah (tidak ada)." as kesimpulan;
        END IF;
	END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `insertKaryawan` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `insertKaryawan`(NIP varchar(20),
Nama varchar(300),
NIK varchar(30),
email varchar(100), 
Tempat_Lahir varchar(50), 
Tanggal_Lahir date, 
Alamat varchar(250), 
NPWP varchar(200),
BPJS_TenagaKerja varchar(250), 
BPJS_Kesehatan varchar(250) ,
Create_at date ,
Foto longtext ,
Id_Jabatan int(11) ,
Alamat_Kerja varchar(250) ,
Telp_Kerja varchar(200),
Create_by int)
BEGIN
	declare count int;-- 0 berlum ada, 1 ada
    declare id int;
    
    -- cek apakah ada karyawan dengan nip seperti param, 0 belum ada, 1 sudah ada delete_at = null
    select count(nip) into count from karyawan where karyawan.nip like nip and Delete_at is not null;
        
	if count = 0
    then
		
		-- aman
		-- insert karyawan
		insert into karyawan (NIP, Nama, NIK, email, Tempat_Lahir , Tanggal_Lahir,  Alamat, NPWP, BPJS_TenagaKerja, BPJS_Kesehatan,
		Create_at, Foto, Id_Jabatan, Alamat_Kerja, Telp_Kerja) 
        values (NIP, Nama, NIK, email, Tempat_Lahir , Tanggal_Lahir,  Alamat, NPWP, BPJS_TenagaKerja, BPJS_Kesehatan,
		Create_at , Foto, Id_Jabatan ,Alamat_Kerja ,Telp_Kerja);
		-- insert log karyawan
        insert into log(Keterangan,Create_at,Create_by) values (concat('Memasukkan data karyawan dengan NIP : ',NIP),curdate(),create_by);
        
        -- insert user
        insert into user (email,password,Role,Create_at,Id_Karyawan) values (email,'password',1,curdate(),NIP);
        
        select User.Id_User into id from User Where User.email like email;
        
        -- insert log user
        insert into log(Keterangan,Create_at,Create_by) values (concat('Memasukkan data User dengan Id_User : ',id),curdate(),Create_by);
	else
		-- update karyawan
		update karyawan  set Create_at = curdate(), Update_at=curdate(), Delete_at = null where NIP like NIP;
        update user set Create_at = curdate(), Update_at=curdate(), Delete_at = null where NIP like NIP;
        -- log karyawan
        insert into log(Keterangan,Create_at,Create_by) values(concat('Update data karyawan dengan NIP : ',NIP),curdate(),Create_by);
        
        -- update user
        update user set Delete_at = null,password = 'password' where NIP like NIP;
        -- log user
        select User.Id_User into id from User Where User.email like email;
        insert into log(Keterangan,Create_at,Create_by) values(concat('Update data user dengan NIP : ',id),curdate(),Create_by);
        
    end if;
	if count = 0
    then
		select 'Berhasil menambahkan data Karyawan';
	else
		select 'Berhasil memperbaharui data karyawan';
    end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `insertProyek` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `insertProyek`(
Nama varchar(100),
Tanggal_M date,
Tanggal_S date,
Status varchar(20),
Klien varchar(50),
Kategori varchar(50),
Create_by int)
BEGIN
	declare count,id_p int;
    
    select count(Id_Proyek) into count from proyek where Nama_Proyek like Nama and (Delete_at is not null or Delete_at is null);
    -- 0 belum ada, 1 udah ada delete_at =  null
    
    if count =0
    then
		insert into proyek (Nama_Proyek,Tanggal_Mulai,Tanggal_Selesai,Status,Klien,Kategori,Create_at) 
        values(Nama,Tanggal_M,Tanggal_S,Status,Klien,Kategori,curdate());
        
        select Id_proyek into id_p from proyek where Nama_Proyek like Nama;
        insert into log(Keterangan,Create_at,Create_by) values(concat("Menambahkan Proyek dengan Id_Proyek : ",id_p),curdate(),Create_by);
    end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `insertRekening` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `insertRekening`(
Nomor int,
NIP varchar(20),
Create_by int)
BEGIN
	declare count,id_r int;
    
    select count(Id_Rekening) into count from rekening where Nomor_Rekening = Nomor and (Delete_at is not null or Delete_at is null);
    -- 0 belum ada, 1 udah ada delete_at =  null
    -- asumsi nomor rekening untuk karywan selalu baru, walaupun karyawan tsb sudah pernah terdaftar (keluar kantor -> masuk lagi)
    if count = 0
    then
		insert into rekening(Nomor_Rekening,Create_at,NIP) values(Nomor,curdate(),NIP);
        
        select Id_Rekening into id_r from rekening where Nomor_Rekening = Nomor;
        insert into log(Keterangan,Create_at,Create_by) values(concat('Menambahkan nomor rekening dengan Id_Rekening : ',id_r),curdate(),Create_by);
    else if count = 1
    then
		update rekening set Create_at = curdate(), Update_at=curdate(), delete_at = null where Nomor_Rekening = Nomor;
        
        select Id_Rekening into id_r from rekening where Nomor_Rekening = Nomor;
        insert into log (Keterangan,Create_at,Create_by) values(concat('Update rekening (delete_at) dengan Id_Rekening : ',id_r),curdate(),Create_by);
    end if;
    end if;
    
    select count;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `insertStatusKaryawan` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `insertStatusKaryawan`(
Nama varchar(50),
Create_by int)
BEGIN
	declare count,id_s int;
    
    select count(Id_Status) into count from status_karyawan where Nama_Status like Nama and (Delete_at is not null or Delete_at is null);
    -- 0 belum ada, 1 udah ada delete_at =  null
    
    if count = 0
    then
		insert into status_karyawan (Nama_Status,Create_at) values(Nama,curdate());
        
        select Id_Status into id_s from status_karyawan where Nama_Status like Nama;
        insert into log(Keterangan,Create_at,Create_by) values(concat('Menambahkan Status Karyawan dengan Id_Status : ',id_s),curdate(),Create_by);
    else if count = 1
    then
		update status_karyawan set Create_at = curdate(), Update_at = curdate(), Delete_at = null where Nama_Status like Nama;
        
        select Id_Status into id_s from status_karyawan where Nama_Status like Nama;
        insert into log(Keterangan,Create_at,Create_by) values(concat('Update Status Karyawan dengan Id_Status : ',id_s),curdate(),Create_by);
    end if;
    end if;
    
    select count;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `insertTodoList` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `insertTodoList`(
Nama varchar(50),
Desk varbinary(250),
Status_Task varchar(20),
Create_by int)
BEGIN
	declare count,id_t int;
    
    select count(Id_Task) into count from todo_list where Nama_Task like Nama and (Delete_at is null or Delete_at is not null);
    
    if count = 0
    then
		insert into todo_list(Nama_Task,Deskripsi,Status_Task,Create_at,Id_User) values(Nama,Desk,Status_Task,curdate(),Create_by);
        
        select Id_Task into id_t from todo_list where Nama_Task like Nama;
        insert into log(Keterangan,Create_at,Create_by) values(concat('Menambahkan Todo List dengan Id_Task : ',id_t),curdate(),Create_by);
    end if;
    
	select count;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `insertUser` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `insertUser`(
email_ varchar(100),
Create_by int)
BEGIN
	declare count,id_u int;
    select count(Id_User) into count from user where email like email_ and (Delete_at is not null or Delete_at is null); -- 1 sudah ada, 0 belum ada delete_at null 
    
	if count=0
    then
		insert into user (email,Password,Role,Create_at) values(email_,'password',0,curdate());
        
        select Id_User into id_u from user where email like email_;
        insert into log(Keterangan,Create_at,Create_by) values(concat("Menambahkan admin dengan Id_User : ",id_u),curdate(),Create_by);
	else if count =1
    then
		-- asumsi email -> delete_at = null
		update user set Create_at = curdate(), Update_at=curdate(), delete_at = null and password = "password" where email like email_;
        
        select Id_User into id_u from user where email like email_;
        insert into log(Keterangan,Create_at,Create_by,Id_User) values(concat("Merubah delete_at menjadi null dan merubah password menjadi default dengan Id_User : ",id_u),curdate(),Create_by,id_u);
    end if;
    end if;
    select count;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `updateDepartemen` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `updateDepartemen`(
Id_D int,
Nama varchar(50),
Create_by int)
BEGIN
	declare count int;
    
    select count(Id_Departemen) into count from departemen where Id_Departemen = Id_D and delete_at is null;
    
    if count = 1
    then
		update departemen set Nama_Departemen = Nama, Update_at=curdate() where Id_Departemen = Id_D;
        
        insert into log(Keterangan,Create_at,Create_by) values(concat("Update nama Departemen dengan Id_Departemen : ",Id_D),curdate(),Create_by);
    end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `UpdateJabatan` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `UpdateJabatan`(nama_jab_Lama varchar(50), nama_jab_Baru varchar(50), id_User int(11), id_Depart int(11), lev int(11))
BEGIN
	DECLARE cekNamaJabatan int(11);
    DECLARE deleteStatus date;
    DECLARE eksisDepartemen int(11);
    DECLARE update_by int(11);
    DECLARE hasil varchar(1000);
    DECLARE kesimpulan varchar(200);
    
    -- PADA UPDATE jABATAN INI PATOKANNYA PADA ID JABATAN. BUKAN PADA NAMA jABATAN, KARENA NAMA JABATAN KEMUNGKINAN BISA DIUPDATE JUGA--
    SELECT Id_Jabatan  INTO cekNamaJabatan from jabatan WHERE Nama_Jabatan = nama_jab_Lama;
	SELECT Id_Departemen from departemen where departemen.Id_Departemen = id_Depart into eksisDepartemen;
    SELECT id_User into update_by;
    SELECT "Berhasil merubah : " INTO kesimpulan;
    
    IF cekNamaJabatan is not null
    THEN
		SELECT Delete_at from jabatan where jabatan.Nama_Jabatan = nama_jab_Lama into deleteStatus;
        IF deleteStatus is null
        THEN
			IF nama_jab_Baru IS NOT NULL
            THEN
				UPDATE jabatan 
				SET Nama_Jabatan = nama_jab_Baru , Update_at = curdate()
                WHERE jabatan.Id_Jabatan = cekNamaJabatan;
                select CONCAT(kesimpulan ," Nama Jabatan, ") into kesimpulan;
                select concat("Update Jabatan lama dengan ID Jabatan :", cekNamaJabatan , " dengan nama baru : " , nama_jab_Baru ,".") INTO HASIL;
                -- insert history --
				call `InsertHistory`(update_by, hasil);
                -- call SP Insert Divisi_departemen -- 
            END IF;
            IF id_Depart IS NOT NULL
            THEN
				IF eksisDepartemen IS NOT NULL
                THEN
					UPDATE jabatan 
					SET Id_Departemen = id_Depart , Update_at = curdate()
					WHERE jabatan.Id_Jabatan = cekNamaJabatan;
                    select CONCAT(kesimpulan ," ID Departemen pada Jabatan, ") into kesimpulan;
					select concat("Update Jabatan lama dengan ID Jabatan :", cekNamaJabatan , " dengan Id Departemen baru : " , id_Depart ,".") INTO HASIL;
					-- insert history --
					call `InsertHistory`(update_by, hasil);
					-- call SP Insert Divisi_departemen -- 
                END IF;
            END IF;
            IF lev IS NOT NULL
            THEN
				UPDATE jabatan 
				SET Level = lev , Update_at = curdate()
                WHERE jabatan.Id_Jabatan = cekNamaJabatan;
                select CONCAT(kesimpulan ," Level Jabatan, ") into kesimpulan;
                select concat("Update Jabatan lama dengan ID Jabatan :", cekNamaJabatan , " dengan Level baru : " , lev ,".") INTO HASIL;
                -- insert history --
				call `InsertHistory`(update_by, hasil);
                -- call SP Insert Divisi_departemen -- 
            END IF;
            select substring(kesimpulan, 1, length(kesimpulan)-2) into kesimpulan;
            select kesimpulan;
		ELSE
                select "Jabatan tidak ditemukan atau sudah dihapus." into kesimpulan;
                select kesimpulan;
		END IF;
    ELSE
		SELECT concat("Tidak ada Jabatan dengan nama : ", nama_jab_Lama) into kesimpulan;
        select kesimpulan;
    END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `UpdateKaryawan` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `UpdateKaryawan`(update_by int, idChange varchar(20), namaKaryawan varchar(300), email varchar(100),
 foto longtext, Tanggal_Lahir date, address varchar(250),
Tempat_Lahir varchar(50), NPWP varchar(200), BPJS_TenagaKerja varchar(250), BPJS_Kesehatan varchar(250),
Id_Div INT(11), Id_Depart INT(11), Id_Jab INT(11), Alamat_Kerja varchar(250), Telp_Kerja varchar(200))
BEGIN
	DECLARE findKaryawan varchar(20);
    DECLARE findKaryawanAktive varchar(20);
    DECLARE keteranganUpdate varchar(200);
    DECLARE name_divisi varchar(50);
    DECLARE name_departemen varchar(50);
    DECLARE name_jabatan varchar(50);
    DECLARE hasil varchar(1000);
    
    
    select karyawan.NIP into findKaryawan from karyawan where karyawan.NIP like idChange;
    select "" into keteranganUpdate;
    -- tidak ada karyawan dengan nip tersebut--
    if findKaryawan = null
    THEN
		Select "Belum ada Karyawan dengan NIP : " + idChange;
	else
		 select karyawan.NIP into findKaryawanAktive from karyawan where bts.karyawan.NIP like idChange and bts.karyawan.Delete_at is null;
        -- ada karyawan dengan nip tsb dan masih aktive--
		if findKaryawanAktive is not null
        THEN
			if namaKaryawan IS NOT NULL
			THEN UPDATE karyawan
				SET Nama = namaKaryawan ,Update_at = curdate()
				WHERE karyawan.NIP like idChange;
			
            select concat(keteranganUpdate, "Nama Karyawan , ") INTO keteranganUpdate;
            select concat("Update nama karyawan lama dengan NIP :", idChange , " dengan nama baru : " , namaKaryawan ,".") INTO HASIL;
            -- insert history --
            call `InsertHistory`(update_by, hasil);
			END IF;
			
			if email IS NOT NULL
			THEN UPDATE karyawan
					SET karyawan.email = email , Update_at = curdate()
					WHERE bts.karyawan.NIP like idChange;
			select concat(keteranganUpdate, "E-mail , ") into keteranganUpdate;
            -- insert history --
            select concat("Update email karyawan lama dengan NIP :",idChange," dengan email baru : ",email,".") into hasil;
			call `InsertHistory`(update_by, hasil);
			END IF;        
			
			if foto IS NOT NULL
				THEN 
					UPDATE karyawan
					SET Foto = foto , Update_at = curdate()
					WHERE karyawan.NIP like idChange;
			select concat(keteranganUpdate, "Foto , ") INTO keteranganUpdate;
            select concat("Update foto karyawan lama dengan NIP :",idChange," dengan foto baru : ",foto," .") into hasil;
            -- insert history --
            call `InsertHistory`(update_by, hasil);
			END IF; 
            
            if Tanggal_Lahir IS NOT NULL
				THEN 
					UPDATE karyawan
					SET Tanggal_Lahir = Tanggal_Lahir , Update_at = curdate()
					WHERE karyawan.NIP like idChange;
			select concat(keteranganUpdate, "Tanggal Lahir , ") INTO keteranganUpdate;
            select concat("Update tanggal lahir karyawan lama dengan NIP :",idChange," dengan tanggal lahir baru : ",Tanggal_Lahir,".") into hasil;
            -- insert history --
            call `InsertHistory`(update_by, hasil);
			END IF;  
            
            if address IS NOT NULL
				THEN UPDATE karyawan
					SET Alamat = address , Update_at = curdate()
					WHERE karyawan.NIP like idChange;
            select concat(keteranganUpdate, "Alamat , ") INTO keteranganUpdate;
            select concat("Update alamat karyawan lama dengan NIP :",idChange," dengan alamat baru : ",address," .") into hasil;
            -- insert history --
            call `InsertHistory`(update_by, hasil);
			END IF;
            
            if Tempat_Lahir IS NOT NULL
				THEN UPDATE karyawan
					SET Tempat_Lahir = Tempat_Lahir , Update_at = curdate()
					WHERE karyawan.NIP like idChange;
            select concat(keteranganUpdate, "Tempat Lahir , ") INTO keteranganUpdate;
			select concat("Update tempat lahir karyawan lama dengan NIP :",idChange," dengan tempat lahir baru : ",Tempat_Lahir," .") into hasil;
            -- insert history --
            call `InsertHistory`(update_by, hasil);

			END IF;
            
            if NPWP IS NOT NULL
				THEN UPDATE karyawan
					SET NPWP = NPWP , Update_at = curdate()
					WHERE karyawan.NIP like idChange;
            select concat(keteranganUpdate, "NPWP , ") INTO keteranganUpdate;
            select concat("Update NPWP karyawan lama dengan NIP :",idChange," dengan NPWP baru : ",NPWP," .") into hasil;
            -- insert history --
           call `InsertHistory`(update_by, hasil);
			END IF;
            
            if BPJS_TenagaKerja IS NOT NULL
				THEN UPDATE karyawan
					SET BPJS_TenagaKerja = BPJS_TenagaKerja , Update_at = curdate()
					WHERE karyawan.NIP like idChange;
            select concat(keteranganUpdate, "BPJS Tenaga Kerja , ") INTO keteranganUpdate;
            select concat("Update BPJS Tenaga Kerja karyawan lama dengan NIP :",idChange," dengan BPJS Tenaga Kerja baru : ",BPJS_TenagaKerja," .") into hasil;
            -- insert history --
            call `InsertHistory`(update_by, hasil);
			END IF;
            
            if BPJS_Kesehatan IS NOT NULL
				THEN UPDATE karyawan
					SET BPJS_Kesehatan = BPJS_Kesehatan , Update_at = curdate()
					WHERE karyawan.NIP like idChange;
            select concat(keteranganUpdate, "BPJS Kesehatan , ") INTO keteranganUpdate;
            select concat("Update BPJS Kesehatan karyawan lama dengan NIP :",idChange," dengan BPJS Kesehatan baru : ",BPJS_Kesehatan," .") into hasil;
            -- insert history --
            call `InsertHistory`(update_by, hasil);
			END IF;
            
            if Id_Div IS NOT NULL
				THEN UPDATE karyawan
					SET Id_Divisi = Id_Div , Update_at = curdate()
					WHERE karyawan.NIP like idChange;
            select concat(keteranganUpdate, "Id Divisi , ") INTO keteranganUpdate;
            select Nama_Divisi into name_divisi from divisi where divisi.Id_Divisi = Id_Div;
            select concat("Update Id Divisi karyawan lama dengan NIP :",idChange," dipindahkan ke Divisi : ",name_divisi," .") into hasil;
            -- insert history --
           call `InsertHistory`(update_by, hasil);
			END IF;
            
            if Id_Depart IS NOT NULL
				THEN UPDATE karyawan
					SET Id_Departemen = Id_Depart , Update_at = curdate()
					WHERE karyawan.NIP like idChange;
            select concat(keteranganUpdate, "Id Departemen , ") INTO keteranganUpdate;
            select Nama_Departemen into name_departemen from departemen where departemen.Id_Departemen = Id_Depart;
            select concat("Update Id Departemen karyawan lama dengan NIP :",idChange," dipindahkan ke Departemen : ",name_departemen," .") into hasil;
            -- insert history --
            call `InsertHistory`(update_by, hasil);

			END IF;
            
            if Id_Jab IS NOT NULL
				THEN UPDATE karyawan
					SET Id_Jabatan = Id_Jab , Update_at = curdate()
					WHERE karyawan.NIP like idChange;
            select concat(keteranganUpdate, "Id Jabatan , ") INTO keteranganUpdate;
            select Nama_Jabatan into name_jabatan from jabatan where jabatan.Id_Jabatan = Id_Jab;
            select concat("Update Id jabatan karyawan lama dengan NIP :",idChange," dipindahkan ke Jabatan : ",name_jabatan," .") into hasil;
            -- insert history --
            call `InsertHistory`(update_by, hasil);
			END IF;
            
            if Alamat_Kerja IS NOT NULL
				THEN UPDATE karyawan
					SET Alamat_Kerja = Alamat_Kerja , Update_at = curdate()
					WHERE karyawan.NIP like idChange;
            select concat(keteranganUpdate, "Alamat Kerja , ") INTO keteranganUpdate;
            select concat("Update Alamat kerja karyawan lama dengan NIP :",idChange," dengan Alamat Kerja baru : ",Alamat_Kerja," .") into hasil;
             -- insert history --
            call `InsertHistory`(update_by, hasil);
			END IF;
			
            if Telp_Kerja IS NOT NULL
				THEN UPDATE karyawan
					SET Telp_Kerja = Telp_Kerja , Update_at = curdate()
					WHERE karyawan.NIP like idChange;
            select concat(keteranganUpdate, "Telp Kerja , ") INTO keteranganUpdate;
            select concat("Update Telp kerja karyawan lama dengan NIP :",idChange," dengan Nomor Telpon Kerja baru : ",Telp_Kerja," .") into hasil;
            -- insert history --
            call `InsertHistory`(update_by, hasil);
			END IF;
			Select * , "UPDATED SUCCESSFULLY" as statsu, keteranganUpdate FROM karyawan WHERE karyawan.NIP like idChange;
	   else
			select concat("Karyawan dengan NIP : ",idChange," Sudah tidak begabung dalam Perusahaan.") into hasil;
			Select * , "UPDATE FAILED" as statsu, hasil FROM karyawan WHERE karyawan.NIP like idChange;
       END IF;
	END IF;   
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `updateProyek` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `updateProyek`(
Id_Proyek_ int,
Nama varchar(100),
Tanggal_S date,
Status varchar(20),
Kategori varchar(50),
Create_by int)
BEGIN
	declare count int;
    
    select count(Id_Proyek) into count from proyek where Id_Proyek = Id_Proyek_ and delete_at is null;
    
    if count = 1
    then
		update proyek set Nama_Proyek = Nama,Tanggal_Selesai = Tanggal_S, Status = Status,Kategori = Kategori,Update_at = curdate() where Id_Proyek = Id_Proyek_;
        
        insert into log(Keterangan,Create_at,Create_by) values(concat('Update proyek dengan Id_Proyek : ',Id_Proyek_),curdate(),Create_by);
    end if;
    
    select count;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `updateRekening` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `updateRekening`(
Id_R int,
Nomor int,
Create_by int)
BEGIN
	declare count int;
    
    select count(Id_Rekening) into count from rekening where Id_Rekening = Id_R and Delete_at is null;
    
    if count = 1
    then
		update rekening set Nomor_Rekening = Nomor, Update_at = curdate() where Id_Rekening = Id_R;
        
        insert into log(Keterangan,Create_at,Create_by) values (concat('Update rekening dengan Id_Rekeking : ',Id_R),curdate(),Create_by);
    end if;
    
    select count;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `updateStatusKaryawan` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `updateStatusKaryawan`(
Id_S int,
Nama varchar(50),
Create_by int)
BEGIN
	declare count int;
    
    select count(Id_Status) into count from status_karyawan where Id_Status = Id_S and delete_at is null;
    
    if count = 1
    then
		update status_karyawan set Nama_Status = Nama, Update_at = curdate() where Id_Status = Id_S;
    
		insert into log(Keterangan,Create_at,Create_by) values(concat('Update Status Karyawan dengan Id_Status : ',Id_S),curdate(),Create_by);
    end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `updateTodoList` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `updateTodoList`(
Id_T int,
Nama varchar(50),
Desk varchar(250),
Status_Task varchar(20),
Id_User int(11),
Create_by int)
BEGIN
	declare count int;
    
    select count(Id_Task) into count from todo_list where Id_Task = Id_T and Delete_at is null;
    
    if count =1 
    then
		update todo_list set Nama_Task = Nama, Deskripsi = Desk, Status_Task = Status_Task, Id_User = Id_User, Update_at = curdate() where Id_Task = Id_T;
        
        insert into log(Keterangan,Create_at,Create_by) values(concat('Update todo list dengan Id_Task : ',Id_T),curdate(),Create_by);
    end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `updateUser` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `updateUser`(
Id_U int,
Password_ varchar(50),
Create_by int)
BEGIN
    declare Id_K varchar(20);
    declare count int;
    
    select count(Id_User) into count from user where Id_User=Id_U and Delete_at is null;-- 1 ada, 0 ga ada
    
    if count = 1
    then
		update user set Password = Password_ where Id_User = Id_U;
		
		insert into log (Keterangan,Create_at,Create_By) values (concat('Update data user Password dengan Id_User',id_U),curdate(),Create_by);
	end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-08-05 18:57:52
